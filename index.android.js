'use strict';

import React, { AppRegistry } from 'react-native';
import Repuplive from './app/Repuplive';
import Pushy from 'pushy-react-native';

Pushy.listen();
Pushy.setNotificationListener(async (data) => {
  // Print notification payload data
  // console.log(data,'notification data is');
  // console.log('Received notification: ' + JSON.stringify(data));

  // window.notificationpayloaddata = data

  // Notification title

  let notificationTitle = 'Repup';

  let notificationText = data.message || 'Repup notification';

  Pushy.notify(notificationTitle, notificationText);


});

AppRegistry.registerComponent('Counter', () => Repuplive);

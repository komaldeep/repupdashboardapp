import React, {Component} from 'react';
import { Container, Content, ListItem, Text, List } from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, InteractionManager} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/DashboardActions';
import { connect } from 'react-redux';
import GuestFeedbackHeader from '../components/Header/GuestFeedbackHeader';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import GuestFeedbackContainer from '../components/Widget/GuestFeedbackWidgit/GuestFeedbackContainer';
var Realm = require('realm');
import {AfterInteractions} from 'react-native-interactions';
import Button from 'react-native-button';
import { Actions } from 'react-native-router-flux';
import myTheme from '../Theme/Theme';

class GuestFeedback extends Component {

    constructor(props) {
        super(props);
        this.state = {
            renderPlaceholderOnly: true
        };
    }


    componentDidMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });

        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);

        let replies = ["all"];
        let category_mentions = ["all"];

        let date_range = {
            "start":"2017-06-01",
            "end":"2017-06-30",
        };

        let nps = "all";
        let properties  = ["all"];
        let sources= ["feedback"];
        let tags = [];
        let limit = 6;
        let rating = -1;
        let skip = 0;

        this.props.actions.GuestfeedbackList(Authobjects,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
        this.props.actions.Properties(Authobjects);

        InteractionManager.runAfterInteractions(() => {
            this.setState({renderPlaceholderOnly: false});
        });
    }

    GuestFeedback(){
        this.refs['myDrawer'].closeDrawer();
    }

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }


    _renderPlaceholderView() {

        var navigationView = (

            <Container>
                <Content theme={myTheme}>
                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Repup </Text>
                    </View>

                    <List>
                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={Actions.ReviewsFeed}>
                                <Text>Reviews Feed</Text>
                            </Button>
                        </ListItem>


                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={this.GuestFeedback.bind(this)}>
                                <Text>Guest Feedback</Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={Actions.Issuescontainer}>
                                <Text> Issues </Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={this.SignOut.bind(this)}>
                                <Text> Sign Out </Text>
                            </Button>
                        </ListItem>

                    </List>

                </Content>
            </Container>
        );

        return (

            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView}>

            <GuestFeedbackHeader opentheDrawer={this.openDrawer.bind(this)} />

            </DrawerLayoutAndroid>
        );
    }


    SignOut(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });

        realm.write(() => {
            let allobjects = realm.objects('LoginAuth');
            realm.delete(allobjects);
            Actions.Login();
        });

    }

    render() {

        if (this.state.renderPlaceholderOnly == true) {
            return this._renderPlaceholderView();
        }

        var navigationView = (

            <Container>
                <Content theme={myTheme}>
                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Repup </Text>
                    </View>

                    <List>
                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={Actions.ReviewsFeed}>
                                <Text>Reviews Feed</Text>
                            </Button>
                        </ListItem>


                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={this.GuestFeedback.bind(this)}>
                                <Text>Guest Feedback</Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={Actions.Issuescontainer}>
                                <Text> Issues </Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={this.SignOut.bind(this)}>
                                <Text> Sign Out </Text>
                            </Button>
                        </ListItem>

                    </List>

                </Content>
            </Container>
        );

        return (

            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView}>

                <GuestFeedbackHeader opentheDrawer={this.openDrawer.bind(this)} />
                <GuestFeedbackContainer/>

            </DrawerLayoutAndroid>
        );
    }
}


const styles = StyleSheet.create({

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:16,
    },

    menubutton:{
        paddingBottom:10,
        paddingTop:10,
    }

})

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(GuestFeedback);


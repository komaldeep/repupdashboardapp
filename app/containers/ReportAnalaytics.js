import React, {Component} from 'react';
import { Container, Content, List, ListItem, Text} from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, InteractionManager} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/DashboardActions';
import { connect } from 'react-redux';
import ReportAnalyticsHeader from '../components/Header/ReportAnalyticsHeader';
import Report from '../components/Widget/ReportAnalytics/Report';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import Button from 'react-native-button';
var Realm = require('realm');
import {AfterInteractions} from 'react-native-interactions';
import { Actions } from 'react-native-router-flux';
import myTheme from '../Theme/Theme';
import Analyticsmenu from '../components/DrawerMenu/Analytics/Analyticsmenu'

class ReportAnalaytics extends Component {

    constructor(props) {
        super(props);
        this.state = {
            AuthHeader:'',
        };
    }


    componentDidMount(){
        //
        // let realm = new Realm({
        //     schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        // });
        //
        // let backupdata = realm.objects('LoginAuth');
        // let stringtoarray = backupdata[0].LoginAuthKey;
        // let Authobjects = JSON.parse(stringtoarray);
        //
        // let replies = ["all"];
        // let category_mentions= ["all"];
        // let date_range = {
        //     "start":"2017-03-01",
        //     "end":"2017-03-30"
        // }
        //
        // let nps="all";
        // let properties = ["all"];
        // let sources = ["all"];
        // let tags = [];
        // let limit = 6;
        // let rating = -1;
        // let skip = 0;
        //
        //
        // this.props.actions.ReviewsFeeddata(Authobjects,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
        // this.props.actions.Properties(Authobjects);
        //
        //
        //
        // InteractionManager.runAfterInteractions(() => {
        //     this.setState({renderPlaceholderOnly: false});
        // });


    }

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    _renderPlaceholderView() {

        return (
            <View>
                <ReviewsHeader opentheDrawer={this.openDrawer.bind(this)} />
            </View>
        );
    }

    SignOut(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });

        realm.write(() => {
            let allobjects = realm.objects('LoginAuth');
            realm.delete(allobjects);
            Actions.Login();
        });

    }

    render() {

        if (this.state.renderPlaceholderOnly == true) {
            return this._renderPlaceholderView();
        }

        return (

            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => <Analyticsmenu/>}>

                <ReportAnalyticsHeader opentheDrawer={this.openDrawer.bind(this)} />
                <Report/>


            </DrawerLayoutAndroid>
        );
    }
}


const styles = StyleSheet.create({

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,

    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:16,
    },

    FilterBackground: {

    },

    menubutton:{
        // backgroundColor: "black",
        // marginBottom:50,
        paddingBottom:10,
        paddingTop:10,
    },

    Drawerhead:{

    }

})

export default connect(feedbackdata => ({
        feedbackdata : feedbackdata .counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(ReportAnalaytics);


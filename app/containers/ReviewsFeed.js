import React, {Component} from 'react';
import { Container, Content, List, ListItem, Text} from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, InteractionManager} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/DashboardActions';
import { connect } from 'react-redux';
import ReviewsHeader from '../components/Header/FeedbackHeader';
import ReviewsFeedback from '../components/Widget/ReviewsWidget/Feedback';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import Button from 'react-native-button';
var Realm = require('realm');
import {AfterInteractions} from 'react-native-interactions';
import { Actions } from 'react-native-router-flux';
import myTheme from '../Theme/Theme';


class ReviewsFeed extends Component {

    constructor(props) {
        super(props);
        this.state = {
            AuthHeader:'',
        };
    }


    componentDidMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });

        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);

        let replies = ["all"];
        let category_mentions= ["all"];
        let date_range = {
            "start":"2017-06-01",
            "end":"2017-06-30"
        };

        let nps="all";
        let properties = ["all"];
        let sources = ["all"];
        let tags = [];
        let limit = 6;
        let rating = -1;
        let skip = 0;


        this.props.actions.Profile(Authobjects);
        this.props.actions.Team(Authobjects);
        this.props.actions.ReviewsFeeddata(Authobjects,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
        this.props.actions.Properties(Authobjects);


        InteractionManager.runAfterInteractions(() => {
            this.setState({renderPlaceholderOnly: false});
        });

    }

    Issuescontainer(){
        Actions.Issuescontainer();
        this.refs['myDrawer'].closeDrawer();
    }

    Reviewsfeed(){
        // Actions.ReviewsFeed();
        this.refs['myDrawer'].closeDrawer();
    }

    GuestFeedback(){
        Actions.GuestFeedback();
        this.refs['myDrawer'].closeDrawer();
    }

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    _renderPlaceholderView() {

        return (
            <View>
                <ReviewsHeader opentheDrawer={this.openDrawer.bind(this)} />
            </View>
        );
    }

    SignOut(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}},
                {name: 'Usernname', properties: {AuthUsernname: 'string'}}
            ]
        });

        realm.write(() => {
            let allobjects = realm.objects('LoginAuth');
            let Username =  realm.objects('Usernname')
            realm.delete(allobjects);
            realm.delete(Username);
            Actions.Login();
        });

    }

    render() {

        const { taj } = this.props.state;

        if (this.state.renderPlaceholderOnly == true) {
            return this._renderPlaceholderView();
        }

        var navigationView = (
            <Container style={styles.FilterBackground}>
                <Content theme={myTheme}>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Repup </Text>
                    </View>

                    <List>

                        <ListItem>
                        <Button containerStyle={styles.menubutton} light block onPress={this.Reviewsfeed.bind(this)}>
                                <Text>Reviews Feed</Text>
                        </Button>
                        </ListItem>


                        {   taj == false ?
                            <View>
                                <ListItem >
                                    <Button containerStyle={styles.menubutton} light block onPress={Actions.GuestFeedback}>
                                        <Text>Guest Feedback</Text>
                                    </Button>
                                </ListItem>

                                <ListItem>
                                    <Button containerStyle={styles.menubutton} light  block onPress={Actions.Issuescontainer}>
                                        <Text> Issues </Text>
                                    </Button>
                                </ListItem>
                            </View>
                            :
                            null
                        }

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={this.SignOut.bind(this)}>
                                <Text> Sign Out </Text>
                            </Button>
                        </ListItem>

                    </List>
                </Content>
            </Container>
        );

        return (

            <DrawerLayoutAndroid
                                 ref="myDrawer"
                                 drawerWidth={300}
                                 drawerPosition={DrawerLayoutAndroid.positions.Left}
                                 renderNavigationView={() => navigationView}>

                <ReviewsHeader opentheDrawer={this.openDrawer.bind(this)} />
                <ReviewsFeedback navigator={this.props.navigator}/>


            </DrawerLayoutAndroid>
        );
    }
}


const styles = StyleSheet.create({

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,

    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:16,
    },

    FilterBackground: {

    },

    menubutton:{
        // backgroundColor: "black",
        // marginBottom:50,
        paddingBottom:10,
        paddingTop:10,
    },

    Drawerhead:{

    }

})

export default connect(state => ({
        state : state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(ReviewsFeed);


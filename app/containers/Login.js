import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/DashboardActions';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import {
    View,
    ScrollView,
    StyleSheet,
    Text
} from 'react-native';
import LoginHeader from '../components/Header/LoginHeader';
import LoginForm from '../components/Widget/LoginWidgit/LoginForm';
const Dimensions = require('Dimensions');
import FCM, {RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import {FCMEvent} from 'react-native-fcm';

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height - 25,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
};

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            token:'',
        };
    }

    componentWillMount(){
     //  console.log(window.notificationpayloaddata, 'window.notificationpayloaddata');

     //  let data = window.notificationpayloaddata;

     //  console.log(data,'data');
     //  if(data == undefined){
     //     console.log('it comes in if part ');
     //    data = ''
     //  }
     //  else{

     //    console.log('it comes in else part ');
     //     Actions.Issuescontainer();

     //    if(data.message != ''){
     //        Actions.Issuescontainer();
     //    }
     //     else if(data.type == "issues.create"){
     //        Actions.Issuescontainer();
     //    }
     //    else if(data.type == undefined){
     //        Actions.GuestFeedback();
     //    }
     //    else {
     //        Actions.GuestFeedback();
     //    }
     // }

     //  window.notificationpayloaddata='';


    }

    render() {

        return (
            <View>
                <Text> {this.state.token} </Text>

                <View style={styles.loginView}>
                    <LoginHeader />
                    <LoginForm navigator={this.props.navigator}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    loginView: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: viewport.height
    },

})




export default connect(feedbackdata => ({
        feedbackdata : feedbackdata .counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Login)


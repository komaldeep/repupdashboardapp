import React, {Component} from 'react';
import { Container, Content, List, ListItem, Text } from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, InteractionManager} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/DashboardActions';
import { connect } from 'react-redux';
import IssueHeader from '../components/Header/IssuesHeader';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import Issuewidget from '../components/Widget/Issues/IssuesWidgit';
import {AfterInteractions} from 'react-native-interactions';
var Realm = require('realm');
import Button from 'react-native-button';
import { Actions } from 'react-native-router-flux';
import myTheme from '../Theme/Theme';

class Issuescontainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            renderPlaceholderOnly: true
        };
    }




    componentDidMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });

        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);

        this.setState({
            AuthHeader:Authobjects,
        });

        let properties = ["all"];
        let status = "all";
        let assigned_to = ["all"];
        let sources = ["all"];
        let skip = 0;
        let limit = 6;

        this.props.actions.IssuesList(Authobjects , properties , status, assigned_to, sources, skip, limit);
        this.props.actions.Properties(Authobjects);
        this.props.actions.Team(Authobjects);

        InteractionManager.runAfterInteractions(() => {
            this.setState({renderPlaceholderOnly: false});
        });
    }
    

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    GuestFeedback(){
        InteractionManager.runAfterInteractions(() => {
        this.props.navigator.push({
            id:'GuestFeedback'
        });
        });
    }

    _renderPlaceholderView() {


        var navigationView = (
            <Container>
                <Content theme={myTheme}>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Repup </Text>
                    </View>

                    <List>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={Actions.ReviewsFeed}>
                                <Text>Reviews Feed</Text>
                            </Button>
                        </ListItem>


                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={Actions.GuestFeedback}>
                                <Text>Guest Feedback</Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={this.Issuescontainer.bind(this)}>
                                <Text> Issues </Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={this.SignOut.bind(this)}>
                                <Text> Sign Out </Text>
                            </Button>
                        </ListItem>

                    </List>
                </Content>
            </Container>
        );

        return (
            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView}>

                <IssueHeader opentheDrawer={this.openDrawer.bind(this)} />

            </DrawerLayoutAndroid>
        );
    }


    SignOut(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });

        realm.write(() => {
            let allobjects = realm.objects('LoginAuth');
            realm.delete(allobjects);
            Actions.Login();
        });

    }

    Issuescontainer(){
        this.refs['myDrawer'].closeDrawer();
    }

    Reviewsfeed(){
        Actions.ReviewsFeed();
        this.refs['myDrawer'].closeDrawer();
    }

    GuestFeedback(){

        this.refs['myDrawer'].closeDrawer();
    }


    render() {

        if (this.state.renderPlaceholderOnly == true) {
            return this._renderPlaceholderView();
        }

        var navigationView = (
            <Container>
                <Content theme={myTheme}>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Repup </Text>
                    </View>

                    <List>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={Actions.ReviewsFeed}>
                                <Text>Reviews Feed</Text>
                            </Button>
                        </ListItem>


                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={Actions.GuestFeedback}>
                                <Text>Guest Feedback</Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={this.Issuescontainer.bind(this)}>
                                <Text> Issues </Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={this.SignOut.bind(this)}>
                                <Text> Sign Out </Text>
                            </Button>
                        </ListItem>

                    </List>
                </Content>
            </Container>
        );

        return (
            <AfterInteractions>
            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView}>

                <IssueHeader opentheDrawer={this.openDrawer.bind(this)} />
                <Issuewidget />

            </DrawerLayoutAndroid>
            </AfterInteractions>
        );
    }
}


const styles = StyleSheet.create({

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:16,
    },
    menubutton:{
        paddingBottom:10,
        paddingTop:10,
    }

})

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Issuescontainer);


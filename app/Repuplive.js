import { Text, Navigator, TouchableHighlight } from 'react-native';
import React, {Component} from 'react';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import promise from "redux-promise-middleware";
import logger from "redux-logger";

import * as reducers from './reducers';
import ReviewsFeed from './containers/ReviewsFeed';
import CreateIssue from './containers/CreateIssue';
import Issuescontainer from './containers/Issuescontainer';
import GuestFeedback from './containers/GuestFeedback';
import ReportAnalaytics from './containers/ReportAnalaytics';
import Login from './containers/Login';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);
import { Router, Scene } from 'react-native-router-flux';



export default class Repuplive extends Component {


    navigatorRenderScene(route,navigator){
        _navigator = navigator;
        switch (route.id){
            case "Login":
                return(<Login navigator={navigator} title ="Login"/>);
            case "ReviewsFeed":
                return(<ReviewsFeed navigator={navigator} title ="ReviewsFeed"/>)
            case "CreateIssue":
                return(<CreateIssue navigator = {navigator} title="CreateIssue"/> );
            case "Issuescontainer":
                return(<Issuescontainer navigator = {navigator} title="Issuescontainer"/>);
            case "GuestFeedback":
                return(<GuestFeedback navigator = {navigator} title="GuestFeedback"/>);
            case "ReportAnalaytics":
                return(<ReportAnalaytics navigator = {navigator} title="ReportAnalaytics"/>);
        }
    }

  render() {

    return (

        <Provider store={store}>
           <Router>
              <Scene key="root">
                  <Scene key="Login" component={Login} hideNavBar={true} initial={true} />
                  <Scene key="ReviewsFeed" component={ReviewsFeed} hideNavBar={true} type="replace"/>
                  <Scene key="GuestFeedback" component={GuestFeedback} hideNavBar={true}  type="replace" />
                  <Scene key="Issuescontainer" component={Issuescontainer} hideNavBar={true} type="replace" />
                  <Scene key="ReportAnalaytics" component={ReportAnalaytics} hideNavBar={true} type="replace" />
             </Scene>
          </Router>
        </Provider>

    );
  }
}

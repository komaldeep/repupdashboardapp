import React, {Component} from 'react';
import Button from 'apsl-react-native-button';
import {
    StyleSheet,
    Text,
    TextInput,
    Image,
    View,
    ScrollView,

} from 'react-native';
import FitImage from 'react-native-fit-image';
const Dimensions = require('Dimensions');

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height - 25,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height - 25,
};


class LoginHeader extends Component {
    state = {
        hasErrors: false,
        attemptingLogin: false,
        error: '',
        username: '',
        password: '',
    };

    render() {

            return (
                <View>
                    <Image
                        source={require('../../Images/logo_vertical.png')}
                        style={styles.loginView} />
                </View>
            );
    }
}

const styles = StyleSheet.create({

    loginView: {
        backgroundColor: 'white',
        height: viewport.height*0.34,
        width: viewport.width*0.46
    },

})

module.exports = LoginHeader;

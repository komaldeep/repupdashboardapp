import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, TouchableHighlight} from 'react-native';
import { Container, Header, Title, Button, Left, Right, Body, Icon, Spinner  } from 'native-base';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../actions/DashboardActions';
import { connect } from 'react-redux';
// import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';
// import PercentageCircle from 'react-native-percentage-circle';

class IssuesHeader extends Component {

    constructor(props) {
        super(props);
    }


    DrawerOpen(){
        this.props.opentheDrawer();
    }

    render() {
        const { loader } = this.props.state;

        let Sendloadermessage, header;

        if(loader != '') {

            if (loader.loadingaction == false) {
                Sendloadermessage = false;
            }
            else {
                Sendloadermessage = true;
            }
        }


        if(Sendloadermessage == undefined || Sendloadermessage == false){
            header =
                    <Button transparent >
                        <Spinner color='#ffffff' />
                    </Button>

        }
        else {
            header = <View></View>
        }



        return (

            <Header backgroundColor="#02afbd">

                <Button transparent onPress={this.DrawerOpen.bind(this)}>
                    <Icon name="ios-menu" />
                </Button>

                <Title style ={styles.Headerbar}> Issue </Title>

                {header}

            </Header>

        );
    }
}


const styles = StyleSheet.create({

    Headerbar: {
        fontSize:16,
    }

})

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(IssuesHeader);
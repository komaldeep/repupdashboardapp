import React, {Component} from 'react';
import { Container, Content, List, ListItem, Icon} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item,
    Spinner,
    ListView
} from 'react-native';
import CheckBox from 'react-native-check-box';

export default class TeamMember extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Teammember:false,
        }
    }


    TeammemberName(){

        this.setState({
            Teammember:!this.state.Teammember,
        })
        if(this.state.Teammember == false) {
            this.props.Teamname(this.props.item._id);
        }
        else {
            this.props.uncheckedTeamname(this.props.item._id);
        }
    }

    render() {

        if(this.state.Teammember == true) {
            return (
                <ListItem>
                    <TouchableOpacity style={styles.FilterTouch} onPress={this.TeammemberName.bind(this)}>
                        <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                        <Text style={styles.CheckboxText}> {this.props.item.name} </Text>
                    </TouchableOpacity>
                </ListItem>)
        }
        else {
            return (
                <ListItem>
                    <TouchableOpacity style={styles.FilterTouch} onPress={this.TeammemberName.bind(this)}>
                        <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                        <Text style={styles.CheckboxText}> {this.props.item.name} </Text>
                    </TouchableOpacity>
                </ListItem>
            )
        }

    }
}

const styles = StyleSheet.create({

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },
    CheckboxText:{
        marginLeft:6,
    },
    Imagestyle:{
        marginTop:1,
        marginBottom:2,
        opacity:0.8,
    },

    fadebox:{
        opacity:0.8,
    },
})





import React, {Component} from 'react';
import { Container, Content, Button, List, ListItem, Icon} from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, Image, ScrollView, Alert, Text, Picker, Item, Spinner, ListView, TextInput} from 'react-native';
import CheckBox from 'react-native-check-box';
import PopupDialog from 'react-native-popup-dialog';
import Dimensions from 'Dimensions';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import AutoComplete from 'react-native-autocomplete';
import Tags from '../../../components/tags.json';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height - 25,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height-25,
};

class OpenandCloseIssuePopup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Star:false,
            text:'',
            text1:'',
            Tagssearch:[],
            Membersincc:[],
            OverallTags:[],
            Assignedname:'',
            AuthHeader:'',
        }
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });
        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);
        this.setState({
            AuthHeader:Authobjects,
        });

    }

    filtertags(text){

        this.setState({
            text
        });

    }

    SubmitSuccess(){

        let Issueid = this.props.CloseIssueId;
        let ClosingText = this.state.text;

        this.props.actions.closeIssue(this.state.AuthHeader, Issueid, ClosingText);
        this.props.ShowingValuesLocally(Issueid,ClosingText,"close");
        this.props.closeIssueclosepopup();
    }

    CancelPopup(){
        this.props.closeIssueclosepopup();
    }

    


    render() {

        return(

              <View style={styles.createissuesview}>

                    <TextInput
                        style={styles.inputgroupstyle}
                        placeholder='Write the Issue closure Note'
                        underlineColorAndroid="transparent"
                        onChangeText = {this.filtertags.bind(this)}
                        value = {this.state.text} />


                    <View style={styles.FooterButtons}>

                        <Button bordered success
                                onPress={this.SubmitSuccess.bind(this)}
                                style={styles.submitButtons}
                                textStyle={{fontSize: 13, color:'#000'}}>
                            Submit
                        </Button>

                        <Button bordered success
                                onPress={this.CancelPopup.bind(this)}
                                style={styles.submitButtons}
                                textStyle={{fontSize: 13, color:'#000'}} >
                            Cancel
                        </Button>

                    </View>
                </View>



        )
    }
}

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(OpenandCloseIssuePopup);

const styles = StyleSheet.create({

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },

    Emailbadgebackground:{
        flex:1,
        backgroundColor:"#fff",
        flexDirection:"row",
        flexWrap: 'wrap'
    },

    badgeTextStyle:{
        backgroundColor: 'rgba(211,211,211,0.7)',
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginLeft:4,
        marginRight:4,
        marginTop:6,
        height:30,
    },

    BadgeText:{
        color:"#000",
        fontSize:11,
    },

    CheckboxText:{
        marginLeft:6,
    },

    buttonread:{
        flex:0.3,
        opacity:0.4,
    },

    buttonPost:{
        backgroundColor: 'rgba(211,211,211,0.7)',
        marginTop:16,
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        height:30,
    },

    TagsPanel:{
        flex:1,
        flexDirection:"row",
        marginTop:5,
        marginLeft:5,
    },

    Imagestyle:{
        marginTop:1,
        marginBottom:2,
        opacity:0.8,
    },

    buttontextstyle:{
        color:"rgb(0,128,0)",
        fontSize:11,
    },

    fadebox:{
        opacity:0.8,
    },


    createissuesview:{
        flex:1,
        marginLeft:9,
        marginRight:15,
        marginTop:10
    },

    inputgroupstyle:{
        flex:1,
        fontSize:11,
        borderBottomColor: 'black',
        borderBottomWidth: .5,
        marginBottom:9,
    },

    FooterButtons:{
        flex:1,
        flexDirection:"row",
        justifyContent: 'space-between',
        marginTop:20,
    },

    submitButtons:{
        borderColor: '#000',
        borderRadius: 11,
        marginBottom:40,
        marginTop:20,
        backgroundColor:"#fff",
        width: viewport.width* 0.37,
    }

})





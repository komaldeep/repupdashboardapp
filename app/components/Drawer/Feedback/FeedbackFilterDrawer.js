import React, {Component} from 'react';
import { Container, Content, List, ListItem, Icon} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item,
    Spinner,
    ListView
} from 'react-native';
import CheckBox from 'react-native-check-box';

export default class FeedbackFilterDrawer extends Component {

    constructor(props) {
        super(props);
    }


    PropertyName(){
        this.props.propertyname(this.props.item.name);
    }

    Moreabout(){
        this.setState({
            MoreList: true,
        })
    }

    Lessabout(){
        this.setState({
            MoreList: false,
        })
    }

    Alll(){
        console.log('it comes in all');
        this.setState({
            selected: true,
            Agoda: false,
            booking:false,
            Goibibo:false
        })
        this.refs['myDrawer'].closeDrawer();
    }

    Agoda(){
        this.props.actions.AgodaFetch();
        this.setState({
            selected: false,
            Agoda: true,
            booking:false,
            Goibibo:false
        })
        this.refs['myDrawer'].closeDrawer();

    }

    Booking(){
        this.props.actions.BookingFetch();
        this.setState({
            selected: false,
            Agoda: false,
            booking:true,
            Goibibo:false
        })
        this.refs['myDrawer'].closeDrawer();
    }

    Goibibo(){
        this.props.actions.GoibiboFetch();
        this.setState({
            selected: false,
            Agoda: false,
            booking:false,
            Goibibo:true,
        })
        this.refs['myDrawer'].closeDrawer();
    }

    propertynames(propertyname){
        this.props.actions.GoibiboFetch();
        this.setState({
            selected: false,
            Agoda: false,
            booking:false,
            Goibibo:true,
        })
        this.refs['myDrawer'].closeDrawer();
    }

    Expedia(){

    }

    HolidayIQ(){

    }

    Makemytrip(){

    }

    Tripadviser(){

    }

    Zomoto(){

    }

    Tripadviser_restaurant(){

    }

    Facebook(){

    }

    Google(){

    }

    render() {
        return (
            <ListItem>
                <CheckBox key = {this.props.key} onClick={this.PropertyName.bind(this)}  />
                <Text> {this.props.item} </Text>
            </ListItem>
        );
    }
}





import React, {Component} from 'react';
import { Container, Content, List, ListItem, Icon} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item,
    Spinner,
    ListView
} from 'react-native';
import CheckBox from 'react-native-check-box';

export default class Propertyname extends Component {

    constructor(props) {
        super(props);
        this.state = {
          property:false,
        }
    }


    PropertyName(){

        this.setState({
            property:!this.state.property,
        });

        if(this.state.property == false) {
            this.props.propertyname(this.props.item._id);
        }
        else {
            this.props.uncheckedpropertyname(this.props.item._id);
        }

    }


    render() {


            if (this.state.property == true) {
                return (
                    <ListItem>
                        <TouchableOpacity style={styles.FilterTouch} onPress={this.PropertyName.bind(this)}>
                            <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                            <Text style={styles.CheckboxText}> {this.props.item.name} </Text>
                        </TouchableOpacity>
                    </ListItem>
                )
            }
            else {
                return (
                    <ListItem>
                        <TouchableOpacity style={styles.FilterTouch} onPress={this.PropertyName.bind(this)}>
                            <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                            <Text style={styles.CheckboxText}> {this.props.item.name} </Text>
                        </TouchableOpacity>
                    </ListItem>
                )
            }


    }
}

const styles = StyleSheet.create({

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },
    CheckboxText:{
        marginLeft:6,
    },

    Imagestyle:{
        marginTop:1,
        marginBottom:2,
        opacity:0.8,
    },

    fadebox:{
        opacity:0.8,
    },

})





import React, {Component} from 'react';
import { Container, Content, List, ListItem, Icon} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item,
    Spinner,
    ListView
} from 'react-native';
import CheckBox from 'react-native-check-box';

export default class OtaNames extends Component {

    constructor(props) {
        super(props);
    }


    PropertyName(){
        this.props.propertyname(this.props.item.name);
    }

    render() {
        return (
            <ListItem>
                <CheckBox key = {this.props.key} onClick={this.PropertyName.bind(this)}  />
                <Text> {this.props.item} </Text>
            </ListItem>
        );
    }
}





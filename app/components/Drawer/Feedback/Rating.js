import React, {Component} from 'react';
import { Container, Content, List, ListItem, Icon} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item,
    Spinner,
    ListView
} from 'react-native';
import CheckBox from 'react-native-check-box';

export default class Rating extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Star:false,
        }
    }


    StarName(){

        this.setState({
            Star:!this.state.Star,
        });

        if(this.state.Star == false) {
            this.props.Ratingname(this.props.item);
        }
        else {
            this.props.uncheckedRating(this.props.item);
        }

    }


    render() {


        if (this.state.Star == true) {
            return (
                <ListItem>
                    <TouchableOpacity style={styles.FilterTouch} onPress={this.StarName.bind(this)}>
                        <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                        <Text style={styles.CheckboxText}> {this.props.item} Star </Text>
                    </TouchableOpacity>
                </ListItem>
            )
        }
        else {
            return (
                <ListItem>
                    <TouchableOpacity style={styles.FilterTouch} onPress={this.StarName.bind(this)}>
                        <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                        <Text style={styles.CheckboxText}> {this.props.item} Star </Text>
                    </TouchableOpacity>
                </ListItem>
            )
        }


    }
}

const styles = StyleSheet.create({

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },
    CheckboxText:{
        marginLeft:6,
    },

    Imagestyle:{
        marginTop:1,
        marginBottom:2,
        opacity:0.8,
    },

    fadebox:{
        opacity:0.8,
    },

})





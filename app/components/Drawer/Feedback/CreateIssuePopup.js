import React, {Component} from 'react';
import { Container, Content, Button, List, ListItem, Icon} from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, Image, ScrollView, Alert, Text, Picker, Item, Spinner, ListView, TextInput} from 'react-native';
import CheckBox from 'react-native-check-box';
import PopupDialog from 'react-native-popup-dialog';
import Dimensions from 'Dimensions';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import AutoComplete from 'react-native-autocomplete';
import Tags from '../../../components/tags.json';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height - 25,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height-25,
};

class CreateIssuePopup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Star:false,
            text:'',
            text1:'',
            Tagssearch:[],
            Membersincc:[],
            OverallTags:[],
            Assignedname:'',
            AuthHeader:'',
        }
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });
        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);
        this.setState({
            AuthHeader:Authobjects,
        });

    }

    filtertags(text){

        this.setState({
            text
        });

        let filteredTags = Tags.filter(
            (detail) =>{
                return detail.category_tags.toString().toLowerCase().indexOf(text.toString().toLowerCase()) !==  -1
            }
        );

        // let totaltags = []
       let Tagssearch = filteredTags.map((detail) =>{
            return detail
        });

        this.setState({
            Tagssearch
        });

    }

    TagsUsedin(tags){

        this.state.OverallTags.push(tags);

        this.setState({
            OverallTags:this.state.OverallTags,
            text:'',
        })

    }


    AddingEmailbypost(){

        if(this.state.text1 != ''){
        this.state.Membersincc.push(this.state.text1);

           this.setState({
               Membersincc: this.state.Membersincc,
               text1:'',
           });
        }
    }

    SubmitSuccess(){

        const {teamlist} = this.props.state;

        let Teammembersarray = [];

        for (let i in teamlist.Team.data) {
            Teammembersarray.push(teamlist.Team.data[i]);
        }

        let Memberid;
        Teammembersarray.map((detail, i) => {

            if(detail.name == this.state.Assignedname) {
                Memberid = detail._id;
            }

        });

        let ccids = this.state.Membersincc;
        let assigned_to = Memberid;
        let review_id = this.props.Issueid;
        let source = this.props.Source;
        let tags = this.state.OverallTags;
        let status = "open";

        // console.log(assigned_to, 'assigned_to');
        // console.log(this.state.Membersincc, 'this.state.Membersincc');
        // console.log(tags, 'tags');
        // console.log(source, 'source');

        // console.log(review_id, 'review_id');

        this.props.actions.CreateIssue(this.state.AuthHeader,ccids,assigned_to,review_id, source, tags);
        this.props.Closepopupbox("closeit");
        // this.props.actions.OpenandcloseIssue(this.state.AuthHeader ,review_id, status);

    }

    CancelPopup(){
        this.props.Closepopupbox("closeit");
    }



    AddingEmailtoCC(Membername){

        const {teamlist} = this.props.state;

        let Validmembers, Validmembers1;
        let allvalidtemembers = [];
        let Teammembersarray = [];

        for (let i in teamlist.Team.data) {
            Teammembersarray.push(teamlist.Team.data[i]);
        }

        let Memberemailaddress;
        Teammembersarray.map((detail, i) => {
            if(detail.name == Membername) {
                Memberemailaddress = detail.email;
            }
        });

        this.state.Membersincc.push(Memberemailaddress);

        this.setState({
            Membersincc: this.state.Membersincc
        });

    }

    DelatingEmailtoCC(Memberemail){

        let idx = this.state.Membersincc.indexOf(Memberemail);
        this.state.Membersincc.splice(idx, 1);

        this.setState({
            Membersincc: this.state.Membersincc
        })

    }

    DelatingTags(tagsname){

        let idx = this.state.OverallTags.indexOf(tagsname);
        this.state.OverallTags.splice(idx, 1);

        this.setState({
            OverallTags: this.state.OverallTags
        });

    }

    AssignToTask(name){

        this.setState({
            Assignedname: name
        })

    }






    render() {


        const {teamlist} = this.props.state;
        let Teammembers, Teamemails;
        let Teammembersarray = [];


        if(teamlist ==  false){
            Teammembers =
                <ListItem>
                    <Text> Loading ... </Text>
                </ListItem>
        }
        else {

            for (let i in teamlist.Team.data) {
                Teammembersarray.push(teamlist.Team.data[i]);
            }

            let Validmembers, Validmembers1;
            let allvalidtemembers = [];

            Teammembersarray.map((detail, i) => {

                Validmembers = detail.properties.includes(this.props.Propertyid);
                Validmembers1 = detail.properties.includes("all");

                if(detail.active == true){
                    if(Validmembers == true || Validmembers1 == true) {
                           allvalidtemembers.push(detail)
                    }
                }

            });


             let Flatterarray = [];
             if(allvalidtemembers.length != 0){
                Flatterarray.push("Assigned To");
             }

             allvalidtemembers.map((details, i)=> {
                Flatterarray.push(details.name);
             });


            Teammembers = 
            Flatterarray.map((details, i)=> {
                return(
                    <Picker.Item key={i} label={details} value={details} />
                )
            });


            Teamemails = allvalidtemembers.map((details, i)=>{
                return(
                    <Button key={i}
                            small
                            textStyle={styles.BadgeText}
                            style={styles.badgeTextStyle}
                            onPress={this.AddingEmailtoCC.bind(this, details.name)}>
                        {details.name}
                    </Button>
                )

            });


        }


        let ValidMembersaddress = this.state.Membersincc.map((detail, i) => {
            return(
            <Button key={i}
                    small
                    textStyle={styles.BadgeText}
                    style={styles.badgeTextStyle}
                    onPress={this.DelatingEmailtoCC.bind(this, detail)}>
                {detail}
            </Button>
            )
        })


        let Overalltags = this.state.OverallTags.map((detail, i) => {
            return(
                <Button key={i}
                        small
                        textStyle={styles.BadgeText}
                        style={styles.badgeTextStyle}
                        onPress={this.DelatingTags.bind(this, detail)}>
                    {detail}
                </Button>
            )
        })



        let buttontags, buttonscategory , tagsHeader, tagstext;
        // buttonscategory = [];

        this.state.Tagssearch.map((detail, i) => {

           tagsHeader =
               <Container>
                   <Content theme={myTheme}>
               <List>
               <ListItem itemDivider>
                   <Text> {detail.title} </Text>
               </ListItem>
               </List>
                   </Content>
               </Container>

            tagstext= detail.category_tags.map((tags, i) => {
                  return(

                  <Container key={i}>
                      <Content theme={myTheme} key={i}>
                          <List key={i}>
                              <ListItem key={i}>
                                  <Button key={i}
                                      onPress={this.TagsUsedin.bind(this,tags)}
                                      light  block
                                      transparent
                                      style={styles.buttonread}
                                      textStyle={styles.buttontextstyle}>
                                      <Text> {tags} </Text>
                                  </Button>
                              </ListItem>
                          </List>
                      </Content>
                  </Container>
                     )
            });

        });

        if(this.state.text  == ''){
            tagsHeader = <View></View>;
            tagstext= <View></View>;
        }


        return(
                <ScrollView style={styles.alltags}>

                    <View style={styles.createissuesview}>

                        <View style={styles.Emailbadgebackground}>
                        {Overalltags}
                        </View>

                        <View style={styles.TagsPanel}>

                        <TextInput
                            style={styles.inputgroupstyle}
                            placeholder='Enter a service category tag'
                            underlineColorAndroid="transparent"
                            onChangeText={this.filtertags.bind(this)}
                            value={this.state.text} />

                             <Button
                                      onPress={this.TagsUsedin.bind(this,this.state.text)}
                                      transparent
                                      style={styles.buttonPost}
                                      textStyle={styles.buttontextstyle}>
                                      <Text> Post </Text>
                            </Button>

                        </View>



                        {tagsHeader}
                        {tagstext}


                        <Picker
                            selectedValue={this.state.Assignedname}
                            // onValueChange={this.AssignToTask.bind(this)}
                            onValueChange={(Assignedname) => this.setState({Assignedname:Assignedname})} 
                            style={styles.pickerstyle}>

                            {Teammembers}

                        </Picker>

                        <View style={styles.Emailbadgebackground}>
                        {ValidMembersaddress}
                        </View>


                         <View style={styles.TagsPanel}>

                        <TextInput
                            style={styles.inputgroupstyle}
                            placeholder='Keep In CC (Use it to send Email)'
                            underlineColorAndroid="transparent"
                            onChangeText={(text1) => this.setState({text1})}
                            value={this.state.text1} />

                        <Button
                                onPress={this.AddingEmailbypost.bind(this)}
                                transparent
                                style={styles.buttonPost}
                                textStyle={styles.buttontextstyle}>
                                <Text> Post </Text>
                        </Button>

                        </View>

                        <View style={styles.Emailbadgebackground}>
                        {Teamemails}
                        </View>

                        <View style={styles.FooterButtons}>

                        <Button bordered success
                        onPress={this.SubmitSuccess.bind(this)}
                        style={styles.submitButtons}
                        textStyle={{fontSize: 16, color:'#000'}} >
                              Submit
                        </Button>

                        <Button bordered success
                        onPress={this.CancelPopup.bind(this)}
                            style={styles.submitButtons}
                            textStyle={{fontSize: 16, color:'#000'}} >
                            Cancel
                        </Button>

                        </View>
                    </View>

                </ScrollView>


        )
    }
}

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(CreateIssuePopup);

const styles = StyleSheet.create({

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },

    Emailbadgebackground:{
        flex:1,
        backgroundColor:"#fff",
        flexDirection:"row",
        flexWrap: 'wrap'
    },

    badgeTextStyle:{
        backgroundColor: 'rgba(211,211,211,0.7)',
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginLeft:4,
        marginRight:4,
        marginTop:6,
        height:30,
    },

    BadgeText:{
        color:"#000",
        fontSize:11,
    },

    CheckboxText:{
        marginLeft:6,
    },

    buttonread:{
        flex:0.3,
        opacity:0.4,
    },

    buttonPost:{
        backgroundColor: 'rgba(211,211,211,0.7)',
        marginTop:16,
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        height:30,
    },

    TagsPanel:{
        flex:1,
        flexDirection:"row",
        marginTop:5,
        marginLeft:5,
    },

    Imagestyle:{
        marginTop:1,
        marginBottom:2,
        opacity:0.8,
    },

    buttontextstyle:{
        color:"rgb(0,128,0)",
        fontSize:11,
    },

    fadebox:{
        opacity:0.8,
    },

    inputgroupstyle:{
        flex:1,
        fontSize:11,
        borderBottomColor: 'black',
        borderBottomWidth: .5,
        marginBottom:9,
    },

    createissuesview:{
        marginLeft:9,
        marginRight:15,
        marginTop:30,
    },

    FooterButtons:{
        flex:1,
        flexDirection:"row",
        justifyContent: 'space-between'
    },

    submitButtons:{
        borderColor: '#000',
        borderRadius: 11,
        marginBottom:30,
        marginTop:20,
        backgroundColor:"#fff",
        width: viewport.width* 0.4,
    }

})





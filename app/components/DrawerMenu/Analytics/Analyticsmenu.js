import React, {Component} from 'react';
import { Container, Content, List, ListItem, Text} from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, InteractionManager} from 'react-native';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import Button from 'react-native-button';
var Realm = require('realm');
import {AfterInteractions} from 'react-native-interactions';
import { Actions } from 'react-native-router-flux';
import myTheme from '../../../Theme/Theme';


export default class Analyticsmenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            AuthHeader:'',
        };
    }


    Issuescontainer(){
        Actions.Issuescontainer();
        this.refs['myDrawer'].closeDrawer();
    }

    Reviewsfeed(){
        // Actions.ReviewsFeed();
        this.refs['myDrawer'].closeDrawer();
    }

    GuestFeedback(){
        Actions.GuestFeedback();
        this.refs['myDrawer'].closeDrawer();
    }

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    SignOut(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });

        realm.write(() => {
            let allobjects = realm.objects('LoginAuth');
            realm.delete(allobjects);
            Actions.Login();
        });

    }

    render() {

        return (
            <Container style={styles.FilterBackground}>
                <Content theme={myTheme}>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Repup </Text>
                    </View>

                    <List>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={this.Reviewsfeed.bind(this)}>
                                <Text>Reviews Feed</Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light block onPress={Actions.GuestFeedback}>
                                <Text>Guest Feedback</Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={Actions.Issuescontainer}>
                                <Text> Issues </Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={Actions.ReportAnalaytics}>
                                <Text> Analaytics </Text>
                            </Button>
                        </ListItem>

                        <ListItem>
                            <Button containerStyle={styles.menubutton} light  block onPress={this.SignOut.bind(this)}>
                                <Text> Sign Out </Text>
                            </Button>
                        </ListItem>

                    </List>
                </Content>
            </Container>
        );
    }
}


const styles = StyleSheet.create({

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,

    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:16,
    },

    FilterBackground: {

    },

    menubutton:{
        // backgroundColor: "black",
        // marginBottom:50,
        paddingBottom:10,
        paddingTop:10,
    },

    Drawerhead:{

    }

});


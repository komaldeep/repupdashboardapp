import React, {Component} from 'react';
import { Container, Content, Card, CardItem, Icon, Button, InputGroup, Input, List, ListItem, } from 'native-base';
import {StyleSheet,
    View,
    Navigator,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Modal,
    Text,
    TextInput
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';
import CreateIssue from './../../../containers/CreateIssue';
import PopupDialog from 'react-native-popup-dialog';
var moment = require('moment');
const Dimensions = require('Dimensions');

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height,
};


export default class IssueDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            Count:'0',
            ShowLOcalValues:false,
            Localopen:false
        };
    }

    reply(){
        this.setState({
            ShowReply: true,
        });
    }

    createissue(){
        this.props.navigator.push({
            id:'CreateIssue'
        })
    }

    opentagspopup(){
        this.props.openpopupbox(this.props.detail._id);
    }

    OpenDiscuustab(){
        this.setState({
            ShowReply: true,
        })
    }

    CloseIssue(status){

        let closurenote = this.props.detail.closure_note;
        if(closurenote == undefined){
            SendTextValue = '';
        }
        else{
            SendTextValue = this.props.detail.closure_note;
        }



        if(status == "closed"){
              this.props.OpenIssueTag(this.props.detail._id);
        }
        else if(this.props.detail.status == "open"){
            this.props.CloseIssueTag(this.props.detail._id, SendTextValue);
        }
        else {
            this.props.OpenIssueTag(this.props.detail._id);
        }

    }

    SubmitComment(){

        let CommentText = this.state.text;
        let Commentid = this.props.detail.id;

        this.setState({
            SubmitReply: Commentid,
            ShowReply:false
        });
        this.props.PushReviewReply(Commentid , CommentText);

    }


     componentWillReceiveProps(props){

        if(props.detail._id == props.changelocalid ){
       
            this.setState({
                ShowLOcalValues:true,
            });

        }


        if(props.LocalOpenIssueid == props.detail._id){
            console.log('it comes here now');
            this.setState({
                Localopen:true
            })
        }

    }




    render() {

        let ReplyBox, OverallRating, AllRatings, Toprating, showRatingHeader, assignedtoname, User_name, location, Propertyname, Createdbyname;

        if (this.props.detail.review.review_data.rating == undefined) {
            Toprating = false;
        }
        else {
            Toprating = true;
        }


        let ShowClosureNote = false;

        if(this.props.detail.closure_note != undefined){
            ShowClosureNote = true;
        }


        if (this.state.ShowReply == true) {
            ReplyBox =
                <View style={styles.replyplusbuttonview}>
                    <TextInput
                        style={styles.inputgroupstyle}
                        placeholder='Reply your Review Here'
                        underlineColorAndroid="transparent"
                        onChangeText={(text) => this.setState({text})}
                        value={this.state.text}/>
                    <TouchableOpacity onPress={this.SubmitComment.bind(this)} style={styles.Submitcommentbuuton}>
                        <Icon style={styles.submiticonsize} name="md-arrow-dropright-circle"/>
                    </TouchableOpacity>
                </View>
        }
        else {
            ReplyBox = <Text> </Text>
        }


        let TeamMembers = this.props.Teammembersarray;


        if (TeamMembers != []) {

            let filteredMembername = TeamMembers.filter(
                (detail) => {
                    return detail._id == this.props.detail.assigned_to
                }
            );

            if (filteredMembername != undefined) {
                assignedtoname = filteredMembername[0].name;
            }
        }


        let Propertyarray = this.props.Propertyarray;

        if(Propertyarray != []) {
            let filteredproperty = Propertyarray.filter(
                (detail) => {
                    return detail._id == this.props.detail.property_id
                }
            )

            if (filteredproperty != undefined) {
                Propertyname = filteredproperty[0].name;
            }
        }


        if (TeamMembers != []) {
            let filteredCreationname = TeamMembers.filter(
                (detail) => {
                    return detail._id == this.props.detail.created_by
                }
            );

            if (filteredCreationname != undefined) {
                Createdbyname = filteredCreationname[0].name;
            }
        }


        let dateString = this.props.detail.last_updated;
        let date =   moment(dateString).format('DD/MM/YYYY');


        if(this.props.detail.review.reviewer.name != undefined || this.props.detail.review.reviewer.name == ''){
            User_name = <Text> <Icon style={styles.locationiconsize} name='md-contacts'/>{this.props.detail.review.reviewer.name}</Text>
        }

        if(this.props.detail.review.reviewer.location != undefined || this.props.detail.review.reviewer.location == '' ){
            location = <Text> | <Icon style={styles.locationiconsize} name='ios-pin'/>{this.props.detail.review.reviewer.location}</Text>
        }


        let Propertiesarray = this.props.Propertyarray;
        let Hotelname;


        if(Propertiesarray.length != 0) {
            let filteredproperty = Propertiesarray.filter(
                (detail) => {
                    return detail._id == this.props.detail.property_id
                }
            )
            if (filteredproperty != undefined) {
                Propertyname = filteredproperty[0].name;
            }

            Hotelname = <Text style={styles.username}>
                <Icon style={styles.locationiconsize} name='ios-home'/>   {Propertyname}
            </Text>

        }



        let Closebutton;
        let fadeClosingText;
        if(this.props.detail.status == "open"){
            fadeClosingText = false;
            Closebutton = <Text> Close Issue </Text>
        }
        else {
            fadeClosingText = true;
            Closebutton = <Text> Open Issue </Text>
        }

        let status;

        if(this.state.ShowLOcalValues == true){
            ShowClosureNote= false;
            status = "closed";
            Closebutton = <Text> Open Issue </Text>
        }
        else if(this.state.Localopen == true){
            status = "Open";
            Closebutton = <Text> Close Issue </Text>
            fadeClosingText = false;
        }
        else{
           status = this.props.detail.status
        }



        return (
            <Content theme={myTheme}>

                <Card style={styles.FeedbackRow}>

                    <CardItem>
                        <View style={styles.contentHeader}>

                            {Toprating ?
                                <View style={styles.ImageColumn}>
                                    <Text style={styles.OverallRating}>
                                        {this.props.detail.review.review_data.rating.rating}/{this.props.detail.review.review_data.rating.max}
                                    </Text>
                                </View>
                                :
                                <View></View>
                            }


                            <View style={styles.DetaisHeader}>

                                <Text style={styles.titleofotp}>
                                    {this.props.detail.review.review_data.title}
                                </Text>

                                <View style={styles.nameandrating}>
                                    <Text style={styles.username}>
                                         {User_name} {location}
                                    </Text>
                                </View>

                                <Text style={styles.username}>
                                    <Icon style={styles.locationiconsize} name='ios-calendar-outline'/> {date} |  {this.props.detail.review.source}
                                </Text>
                                {Hotelname}
                            </View>
                        </View>


                        <View>
                            <Text style={styles.UserComment}>
                                {this.props.detail.review.review_data.description}
                            </Text>
                        </View>


                        <View style={styles.Listfooter}>
                        <List>
                            <ListItem>

                                <View style={styles.Listviewtable}>
                                     <Text style={styles.listviewfontheader}> Property  </Text>
                                    <Text style={styles.listviewfont}> {Propertyname} </Text>
                                </View>

                            </ListItem>
                            <ListItem>

                                <View style={styles.Listviewtable}>
                                    <Text style={styles.listviewfontheader}> Created  </Text>
                                    <Text style={styles.listviewfont}> {date} </Text>
                                </View>

                            </ListItem>
                            <ListItem>

                                <View style={styles.Listviewtable}>
                                    <Text style={styles.listviewfontheader}> Status  </Text>
                                    <Text style={styles.listviewfont}> {status} </Text>
                                </View>

                            </ListItem>
                            <ListItem>

                                <View style={styles.Listviewtable}>
                                    <Text style={styles.listviewfontheader}> Assigned To  </Text>
                                    <Text style={styles.listviewfont}> {assignedtoname} </Text>
                                </View>

                            </ListItem>
                        </List>
                        </View>


                        <View style={styles.buttonsfooter}>

                                <Button
                                 transparent
                                 style={styles.buttonread}
                                 textStyle={styles.buttontextstyle}
                                 onPress={this.CloseIssue.bind(this, status)}>
                                      <Icon style={styles.locationiconsize} name='md-close' />
                                      {Closebutton}
                                </Button>

                        </View>


                        { ShowClosureNote ?

                         <View style={styles.ReplyTemplete}>
                                        <View style={styles.ReplyTempleteHeader}>
                                            <View style={styles.ImageColumn12}>
                                                <Image source={require('../../../Images/manager2.png')}/>
                                            </View>
                                            <View style={styles.Titleandreply}>
                                                <Text style={styles.titleofotp1}>
                                                    Closure Note
                                                </Text>
                                                {fadeClosingText ? 
                                                <Text style={styles.UserComment}>
                                                    {this.props.detail.closure_note}
                                                </Text>
                                                :
                                                <Text style={styles.UserCommentfade}>
                                                    {this.props.detail.closure_note}
                                                </Text>
                                                }
                                            </View>
                                        </View>
                        </View>
                        :
                        <View></View>
                        }


                         { this.state.ShowLOcalValues ?

                         <View style={styles.ReplyTemplete}>
                                        <View style={styles.ReplyTempleteHeader}>
                                            <View style={styles.ImageColumn12}>
                                                <Image source={require('../../../Images/manager2.png')}/>
                                            </View>
                                            <View style={styles.Titleandreply}>
                                                <Text style={styles.titleofotp1}>
                                                    Closure Note
                                                </Text>
                                             
                                                <Text style={styles.UserComment}>
                                                   {this.props.changelocalText}
                                                </Text>
                                            </View>
                                        </View>
                        </View>
                        :
                        <View></View>
                        }





                    </CardItem>
                </Card>

            </Content>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.1)',
        paddingTop: 20,
    },

    FeedbackRow:{
        flex:1,
        marginLeft:7,
        marginRight:7,
        borderRadius: 5,
        padding: 4,
        shadowColor: '#000000',
        shadowOffset: {
            width: 5,
            height: 9
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        paddingBottom:0,
    },

    contentHeader:{
        flex:1,
        flexDirection:'row',
        marginBottom:10,
    },

    ReplyTemplete:{
        // marginLeft:10,
        marginTop:10,
    },

    ImageColumn12:{
        width: 45,
        height: 40,
        borderRadius: 23,
        backgroundColor: '#fff',
        alignItems:"center",
        justifyContent:"center",
        // marginTop:6,
    },

    ReplyTempleteHeader:{
        flexDirection:'row',
        // backgroundColor:"rgba(0, 0, 0, 0.2)",
    },

    Titleandreply:{
        flex:1,
        flexDirection:'column'
    },

    titleofotp1:{
        fontSize: 13.5,
        // marginLeft: 15,
        fontWeight:"bold",
    },

    ImageColumn:{
        width: 45,
        height: 40,
        backgroundColor:'rgba(0,128,0,0.5)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginTop:6,
    },

    titleofotp:{
        fontSize: 14,
        marginLeft: 15,
        fontWeight:"bold",
    },

    DetaisHeader:{
        flex: 1,
        flexDirection: 'column',
        marginTop:3,
    },

    username:{
        fontSize: 12,
        marginLeft: 15,
        marginTop:2,
    },

    RatingCards:{
        flex: 1,
        flexDirection: 'row',
    },

    OverallRating:{
        color:'#fff',
        fontWeight: 'bold',
        fontSize:11,
    },

    UserComment:{
        opacity:0.9,
        fontSize: 12,
    },

    UserCommentfade:{
        opacity:0.5,
        fontSize: 12,
    },

    Listfooter:{
        marginTop:10,
        backgroundColor: 'rgba(211,211,211,0.11)',
        // paddingBottom:6,
    },

    buttonsfooter:{
        flex:1,
        flexDirection:'row',
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor: 'rgba(211,211,211,0.4)',
        height:22,
        paddingBottom:6,
    },

    buttonread:{
        flex:0.3,
        // fontSize:7,
        alignSelf:"center",
        opacity:0.4,
        justifyContent: 'center',
    },

    Hotel:{
        fontSize:12,
        textAlign: "right",
        fontWeight:'bold',
        color:'blue',
        opacity:0.7,
    },

    texttags:{
        color:'blue',
    },

    buttontextstyle:{
        color:"rgb(0,128,0)",
        fontSize:11,
    },

    listviewfontheader:{
        fontWeight:'bold',
        fontSize:11,
        opacity:0.9
    },

    listviewfont:{
        fontSize:12,
    },

    Listviewtable:{
        flexDirection:'row',
    },

    locationiconsize:{
        fontSize:15,
        color:"#66CCCC"
    },

    optsource:{
        // marginLeft:5,
        width: viewport.width * 0.10,
        alignItems: 'flex-end',
        height:30,
        marginTop:3,
    },

    replyplusbuttonview:{
        flex:1,
        flexDirection:"row",
        // alignItems: 'flex-start',
        justifyContent: 'space-around',
    },

    submiticonsize:{
        fontSize:19,
        color:"#66CCCC"
    },
    inputgroupstyle:{
        flex:1,
        fontSize:11,
    },
    Submitcommentbuuton:{
        justifyContent: 'flex-end',
        marginLeft:7,
        paddingBottom:15,
    },

});





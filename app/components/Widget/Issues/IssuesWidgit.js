import React, {Component} from 'react';
import { Container, Content, List, ListItem,CheckBox, InputGroup, Input, Button} from 'native-base';
import {StyleSheet,View,TouchableOpacity,TouchableHighlight,Image,ScrollView,Alert,Text,Picker,Item} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import IssueDetail from './IssueDetails';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import myTheme from '../../../Theme/Theme';
import PopupDialog from 'react-native-popup-dialog';
import Badge from 'react-native-smart-badge';
import Dimensions from 'Dimensions';
var Realm = require('realm');
import PropertyName from '../../Drawer/Feedback/Propertyname';
import Teammembername from '../../Drawer/Issues/TeamMember'
import Buttons from 'react-native-button';
import OpenandCloseIssuePopup from '../../Drawer/Issues/CloseIssuePopup';
import Openissue from '../../Drawer/Issues/OpenIssue';

class IssuesWidgit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            clicked: false,
            selected1: 'key1',
            language:'',
            loading:6,
            MoreList:false,
            All: false,
            Open:false,
            Closed:false,
            OtpProperties:false,
            SourcesAll: true,
            selected:true,
            Agoda: false,
            booking:false,
            Goibibo:false,
            Expedia:false,
            HolidayIQ:false,
            Makemytrip:false,
            Tripadviser:false,
            Zomoto:false,
            Tripadviser_restaurant:false,
            Facebook:false,
            Google:false,
            Assigned:false,
            Postid:'',
            CloseIssueId:'',
            postids:'',
            comment:'',
            Overallproperties:["all"],
            Properties:["all"],
            AuthHeader:'',
            assigned_to:["all"],
            status:"all",
            loadingDataPush:[],
            AllProperties:true,
            Allteammemers:true,
            closureNoteValue:'',
            changelocalid:'',
            changelocalText: '',
            changelocalmethed:'',
            LocalOpenIssueid:'',
        };
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });
        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);

        this.setState({
            AuthHeader:Authobjects,
        })

    }

    LoadMoreData(){

        let properties = this.state.Properties;
        let status=this.state.status;
        let assigned_to= this.state.assigned_to;
        let sources = this.state.Overallproperties;
        let skip = this.state.loading - 6;
        let limit = 0;

        this.props.actions.IssuesList(this.state.AuthHeader , properties , status, assigned_to, sources, skip, limit);

    }

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    closeDrawer(){
        this.setState({
            clicked: true,
        })
    }

    openPopupBox(Id){

        this.setState({
            Postid: Id
        });

        this.popupDialog.openDialog();

    }

    DiscussPopupBox(){
        this.DiscussupDialog.openDialog();
    }

    handleScroll(event){
        var windowHeight = Dimensions.get('window').height,
            height = event.nativeEvent.contentSize.height,
            offset = event.nativeEvent.contentOffset.y;
        if( windowHeight + offset >= height ){

            let loadingnumber = this.state.loading;
            let addingnumber = 6;
            this.setState({
                loading: loadingnumber+ addingnumber,
            })
            this.LoadMoreData();
        }
    }

    Moreabout(){
        this.setState({
            MoreList: true,
        })
    }

    Lessabout(){
        this.setState({
            MoreList: false,
        })
    }

    GetData(){

        let properties = this.state.Properties;
        let status=this.state.status;
        let assigned_to= this.state.assigned_to;
        let sources = this.state.Overallproperties;
        let skip = 0;
        let limit = 6;

        this.props.actions.IssuesList(this.state.AuthHeader , properties , status, assigned_to, sources, skip, limit);

    }

    All(){
        this.setState({
            All: !this.state.All,
            status:"all"
        })

        if(this.state.All == false){
            this.setState({
                Open: false,
                Closed : false,
            });

            let properties = this.state.Properties;
            let status= "all";
            let assigned_to= this.state.assigned_to;
            let sources = this.state.Overallproperties;
            let skip = 0;
            let limit = 6;

            this.props.actions.IssuesList(this.state.AuthHeader , properties , status, assigned_to, sources, skip, limit);
        }
        this.refs['myDrawer'].closeDrawer();
    }

    Open(){
        this.setState({
            Open: !this.state.Open,
            status:"open",
        });

        if(this.state.Open == false){
            this.setState({
                All: false,
                Closed : false,
            });

            let properties = this.state.Properties;
            let status= "open";
            let assigned_to= this.state.assigned_to;
            let sources = this.state.Overallproperties;
            let skip = 0;
            let limit = 6;

            this.props.actions.IssuesList(this.state.AuthHeader , properties , status, assigned_to, sources, skip, limit);
        }
        this.refs['myDrawer'].closeDrawer();
    }

    Closed(){

        this.setState({
            Closed : !this.state.Closed,
            status:"closed"
        });

        if(this.state.Closed == false){
            this.setState({
                All: false,
                Open: false,
            });

            let properties = this.state.Properties;
            let status="closed";
            let assigned_to= this.state.assigned_to;
            let sources = this.state.Overallproperties;
            let skip = 0;
            let limit = 6;

            this.props.actions.IssuesList(this.state.AuthHeader , properties , status, assigned_to, sources, skip, limit);
        }
        this.refs['myDrawer'].closeDrawer();
    }

    Alll(){

        this.setState({
            selected: !this.state.selected,
            Agoda: false,
            booking:false,
            Goibibo:false,
            Expedia:false,
            HolidayIQ:false,
            Makemytrip:false,
            Tripadviser:false,
            Zomoto:false,
            Tripadviser_restaurant:false,
            Facebook:false,
            Google:false,
            Overallproperties:["all"]
        });


        if(this.state.selected == false) {
            let properties = this.state.Properties;
            let status = this.state.status;
            let assigned_to = this.state.assigned_to;
            let sources = ["all"];
            let skip = 0;
            let limit = 6;

            this.props.actions.IssuesList(this.state.AuthHeader, properties, status, assigned_to, sources, skip, limit);
        }

        this.refs['myDrawer'].closeDrawer();

    }

    Agoda() {

        this.setState({
            selected: false,
            Agoda: !this.state.Agoda,
        })

        if (this.state.Agoda == false) {
            this.state.Overallproperties.push("agoda");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("agoda");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }

    }

    Booking(){

        this.setState({
            selected: false,
            booking: !this.state.booking,
        })

        if (this.state.booking == false) {
            this.state.Overallproperties.push("booking");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("booking");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }

    }

    Goibibo(){

        this.setState({
            selected: false,
            Goibibo: !this.state.Goibibo,
        })

        if (this.state.Goibibo == false) {
            this.state.Overallproperties.push("goibibo");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("goibibo");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }

    }

    propertynames(propertyid){

        this.state.Properties.push(propertyid);
        let idx = this.state.Properties.indexOf("all");

        if (idx != -1) {
            this.state.Properties.splice(idx, 1);
        }
        this.setState({
            Properties: this.state.Properties,
            AllProperties:false
        })
        this.GetData();
        this.refs['myDrawer'].closeDrawer();

    }

    ischeckemptyproperties(){

        if(this.state.Properties == []){
            this.state.Properties.push("all");
            this.setState({
                Properties: this.state.Properties
            })
            this.GetData();
        }

    }

    Teamname(memberid){

        this.state.assigned_to.push(memberid);
        let idx = this.state.assigned_to.indexOf("all");

        if (idx != -1) {
            this.state.assigned_to.splice(idx, 1);
        }
        this.setState({
            Properties: this.state.assigned_to,
            Allteammemers:false,
        });
        this.GetData();
        this.refs['myDrawer'].closeDrawer();

        console.log(this.state.assigned_to);

    }

    uncheckedTeamname(event){

        let idx = this.state.assigned_to.indexOf(event);

        if (idx != -1) {
            this.state.assigned_to.splice(idx, 1);
        }
        this.setState({
            Properties: this.state.assigned_to
        })

        this.GetData();
        // this.ischeckemptyproperties();

    }

    unchecked(event){

        let idx = this.state.Properties.indexOf(event);

        if (idx != -1) {
            this.state.Properties.splice(idx, 1);
        }
        this.setState({
            Properties: this.state.Properties
        })

        this.GetData();
        this.ischeckemptyproperties();

    }

    Expedia(){
        this.setState({
            Expedia: !this.state.Expedia,
            selected: false,
        });

        if (this.state.Expedia == false) {
            this.state.Overallproperties.push("expedia");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("expedia");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    HolidayIQ(){
        this.setState({
            HolidayIQ:!this.state.HolidayIQ,
            selected: false,
        })
        if (this.state.HolidayIQ == false) {
            this.state.Overallproperties.push("holidayiq");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("holidayiq");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Makemytrip(){
        this.setState({
            Makemytrip: !this.state.Makemytrip,
            selected: false
        });

        if (this.state.Makemytrip == false) {
            this.state.Overallproperties.push("makemytrip");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("makemytrip");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Tripadviser(){
        this.setState({
            Tripadviser: !this.state.Tripadviser,
            selected: false,
        });

        if (this.state.Tripadviser == false) {
            this.state.Overallproperties.push("tripadvisor");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("tripadvisor");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Zomoto(){
        this.setState({
            Zomoto:!this.state.Zomoto,
            selected: false,
        })
        if (this.state.Zomoto == false) {

            this.state.Overallproperties.push("zomato");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();

        }
        else {
            let idx = this.state.Overallproperties.indexOf("zomato");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Tripadviser_restaurant(){
        this.setState({
            Tripadviser_restaurant:!this.state.Tripadviser_restaurant,
            selected: false,
        })

        if (this.state.Tripadviser_restaurant == false) {
            this.state.Overallproperties.push("tripadvisor-restaurant");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();

        }
        else {
            let idx = this.state.Overallproperties.indexOf("tripadvisor-restaurant");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Facebook(){
        this.setState({
            Facebook:!this.state.Facebook,
            selected: false,
        })
        if (this.state.Facebook == false) {
            this.state.Overallproperties.push("facebook");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();

        }
        else {
            let idx = this.state.Overallproperties.indexOf("facebook");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Google(){
        this.setState({
            Google:!this.state.Google,
            selected: false,
        })

        if(this.state.Google == false) {
            this.state.Overallproperties.push("google");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }

            this.setState({
                Overallproperties: this.state.Overallproperties
            })

            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("google");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    IssueClosetag(event, closureNoteValue){

        this.ClosetagDialog.openDialog();
        console.log(closureNoteValue,'it reaches and work');
        this.setState({
            CloseIssueId: event,
            closureNoteValue:closureNoteValue
        })
    }

    SureCloseIssue(){
        let closeissueid = this.state.CloseIssueId;
        let body = {"id": closeissueid}
        this.props.actions.Closeissuesurity(body);
    }

    notclose(){
        this.ClosetagDialog.closeDialog();
    }

    PushReviewReply(postids, comment) {

        this.setState({
            postids,
            comment
        });

        let body = {postid: postids, Comment: comment}
        // this.props.actions.PostingReviewsFeedbackBYOwner(body);

    }

    PressAlProperties(){

        if(this.state.AllProperties == false) {
            this.setState({
                Properties: ["all"],
                AllProperties:true,
            });


            let properties = ["all"];
            let status=this.state.status;
            let assigned_to= this.state.assigned_to;
            let sources = this.state.Overallproperties;
            let skip = 0;
            let limit = 6;

            this.props.actions.IssuesList(this.state.AuthHeader , properties , status, assigned_to, sources, skip, limit);
            // this.GetData();
        }
        else {
            this.setState({
                AllProperties:false
            });
        }
        this.refs['myDrawer'].closeDrawer();

    }

    Allteammemers(){

        this.setState({
            Allteammemers:true,
            assigned_to: ["all"]
        });

        let properties = ["all"];
        let status=this.state.status;
        let assigned_to= ["all"];
        let sources = this.state.Overallproperties;
        let skip = 0;
        let limit = 6;

        this.props.actions.IssuesList(this.state.AuthHeader , properties , status, assigned_to, sources, skip, limit);
        this.refs['myDrawer'].closeDrawer();

    }

    closeIssueclosepopup(){
        this.ClosetagDialog.closeDialog();
    }

    closeIssueopenpopup(){
        this.OpenIssuetagDialog.closeDialog();
    }

    OpenIssueTag(CloseIssueId){
        this.setState({
            CloseIssueId
        })
        this.OpenIssuetagDialog.openDialog();
    }

    ShowingValuesLocally(Issueid,ClosingText,methed){

        console.log('it reaches here in ShowingValuesLocally ');
        this.setState({
            changelocalid:Issueid,
            changelocalText: ClosingText,
            changelocalmethed:methed
        })

        console.log(Issueid,ClosingText,methed,'print all these values');

    }

    OpenLocallyIssue(Issueid){

         // console.log('checking issue id in widgit page', Issueid);

        this.setState({
            LocalOpenIssueid:Issueid,
        })

    }



    render() {

        const { Issueslist , teamlist, propertieslist, ClosedIssue, agodalist , bookinglist, goibibolist, Assignedto} = this.props.Issues;
        let ListShow , realobjectlength , OtaList, properties, tags, Teammembersarray, Propertyarray, ReviewsFeedarray;


        if(this.state.MoreList == false){
            OtaList =
                <List>

                    <ListItem itemDivider>
                        <Text> Sources </Text>
                    </ListItem>

                    <ListItem>

                        {this.state.selected ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress={this.Alll.bind(this)}>
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress={this.Alll.bind(this)}>
                                <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>

                        {this.state.Agoda ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress={this.Agoda.bind(this)}>
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Agoda </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress={this.Agoda.bind(this)}>
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Agoda </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.booking ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Booking.bind(this)} >
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Booking </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Booking.bind(this)} >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Booking</Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.Goibibo ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Goibibo.bind(this)} >
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Goibibo </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Goibibo.bind(this)} >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Goibibo </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>
                        <TouchableOpacity style={styles.FilterTouch} onPress={this.Moreabout.bind(this)}>
                            <Text> More... </Text>
                        </TouchableOpacity>
                    </ListItem>

                </List>
        }
        else{
            OtaList =
                <List>
                    <ListItem itemDivider>
                        <Text> Sources </Text>
                    </ListItem>

                    <ListItem>

                        {this.state.selected ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress={this.Alll.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress={this.Alll.bind(this)}>
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>


                        {this.state.Agoda ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress={this.Agoda.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Agoda </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress={this.Agoda.bind(this)}>
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Agoda </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.booking ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Booking.bind(this)} >
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Booking </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Booking.bind(this)} >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Booking</Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.Goibibo ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Goibibo.bind(this)} >
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Goibibo </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Goibibo.bind(this)} >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Goibibo </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>


                    <ListItem>

                        {this.state.Expedia ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Expedia.bind(this)} >
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Expedia </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Expedia.bind(this)} >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Expedia </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.HolidayIQ ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.HolidayIQ.bind(this)}  >
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> HolidayIQ </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.HolidayIQ.bind(this)}  >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> HolidayIQ </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.Makemytrip ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Makemytrip.bind(this)}  >
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Makemytrip </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Makemytrip.bind(this)}  >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Makemytrip </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>


                        {this.state.Tripadviser ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Tripadviser.bind(this)}  >
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Tripadviser </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Tripadviser.bind(this)} >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}>Tripadviser </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.Zomoto ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Zomoto.bind(this)}  >
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Zomoto </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Zomoto.bind(this)} >
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Zomoto </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>


                        {this.state.Tripadviser_restaurant ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Tripadviser_restaurant.bind(this)}>
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Tripadviser restaurant </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Tripadviser_restaurant.bind(this)}>
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Tripadviser restaurant </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.Facebook ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Facebook.bind(this)}>
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Facebook  </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Facebook.bind(this)}>
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Facebook  </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>

                        {this.state.Google ?
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Google.bind(this)}>
                                 <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Google </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Google.bind(this)}>
                                 <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Google </Text>
                            </TouchableOpacity>
                        }

                    </ListItem>

                    <ListItem>
                        <TouchableOpacity style={styles.FilterTouch} onPress={this.Lessabout.bind(this)}>
                            <Text> Less... </Text>
                        </TouchableOpacity>
                    </ListItem>
                </List>
        }

        let Teammembers;
        Teammembersarray = [];
        if(teamlist ==  false){
            Teammembers =
                <ListItem>
                    <Text> Loading ... </Text>
                </ListItem>
        }
        else {
            for (let i in teamlist.Team.data) {
                Teammembersarray.push(teamlist.Team.data[i]);
            }

            Teammembers = Teammembersarray.map((detail, i) => {
                return (
                    <Teammembername key = {i}
                                    item={detail}
                                  //  propertyname={this.propertynames.bind(this)}
                                    Teamname = {this.Teamname.bind(this)}
                                    uncheckedTeamname={this.uncheckedTeamname.bind(this)}

                    />
                )
            })
        }

        Propertyarray = [];
        if(this.props.Issues.propertieslist ==  false){
            properties =
                <ListItem>
                    <Text> Loading ... </Text>
                </ListItem>
        }
        else {
            for (let i in propertieslist.Properties.data) {
                Propertyarray.push(propertieslist.Properties.data[i]);
            }

            properties =
                <List>
                    <ListItem>

                        {
                            this.state.AllProperties ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.PressAlProperties.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.PressAlProperties.bind(this)}>
                                    <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                        }

                    </ListItem>

                    {
                        Propertyarray.map((detail, i) => {
                            return (
                                <PropertyName
                                    key={i}
                                    item={detail}
                                    propertyname={this.propertynames.bind(this)}
                                    uncheckedpropertyname={this.unchecked.bind(this)}
                                />
                            )
                        })
                    }
                </List>
        }


        var navigationView = (
            <Container style={styles.filterbackground}>
                <Content theme={myTheme} >

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Filters </Text>
                    </View>

                    <List>

                        <ListItem itemDivider>
                            <Text > Status </Text>
                        </ListItem>

                        <ListItem>
                            {this.state.All ?
                                <TouchableOpacity style={styles.FilterTouch}  onPress = {this.All.bind(this)} >
                                     <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch}  onPress = {this.All.bind(this)} >
                                     <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                            }
                        </ListItem>

                        <ListItem>

                            {this.state.Open ?
                                <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Open.bind(this)} >
                                     <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Open </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Open.bind(this)} >
                                     <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Open </Text>
                                </TouchableOpacity>
                            }


                        </ListItem>

                        <ListItem>

                            {this.state.Closed ?
                                <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Closed.bind(this)} >
                                     <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Closed </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Closed.bind(this)} >
                                     <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Closed </Text>
                                </TouchableOpacity>
                            }

                        </ListItem>

                        {OtaList}

                        <ListItem itemDivider>
                            <Text> Properties </Text>
                        </ListItem>
                        {properties}

                        <ListItem itemDivider>
                            <Text> Assign To- </Text>
                        </ListItem>

                        <ListItem>

                            {this.state.Allteammemers ?
                                <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Allteammemers.bind(this)} >
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch}  onPress = {this.Allteammemers.bind(this)} >
                                    <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                            }

                        </ListItem>

                        {Teammembers}

                    </List>

                </Content>
            </Container>
        );

        let realm = new Realm({
            schema: [{name: 'Issuebackdata', properties: {Issuebackappdata: 'string'}}]
        });




        if (Issueslist == false) {

            let backupdata = realm.objects('Issuebackdata');
            realobjectlength = <View style={styles.ViewLoading}>
                <Text style={styles.loading}>Loading... </Text>
            </View>


        }
        else {


            realobjectlength = <View></View>;

            let ReviewsIssueslist = [];
            for (let i in Issueslist.Issuesreviewsfeeddata.data) {
                ReviewsIssueslist.push(Issueslist.Issuesreviewsfeeddata.data[i]);
            }



            if (ReviewsIssueslist.length == 0) {
                ListShow = <View style={styles.ViewLoading}>
                    <Text> Sorry, No Review is available  </Text>
                </View>
            }

            else {

                ListShow =
                    ReviewsIssueslist.map((detail, i) => {
                        return (
                            <IssueDetail
                                PushReviewReply={this.PushReviewReply.bind(this)}
                                CloseIssueTag={this.IssueClosetag.bind(this)}
                                OpenIssueTag={this.OpenIssueTag.bind(this)}
                                Discussopenpopupbox={this.DiscussPopupBox.bind(this)}
                                openpopupbox={this.openPopupBox.bind(this)}
                                navigator={this.props.navigator}
                                detail={detail}
                                Teammembersarray={Teammembersarray}
                                Propertyarray={Propertyarray}
                                key={i}
                                changelocalid={this.state.changelocalid}
                                changelocalText= {this.state.changelocalText}
                                changelocalmethed = {this.state.changelocalmethed}
                                LocalOpenIssueid={this.state.LocalOpenIssueid}
                            />
                        )
                    })
            }
        }


        if(this.state.Postid == ''){
            tags = <View></View>
        }
        else {
            filteretags = Issueslist.Issuesreviewsfeeddata.filter(
                (detail) =>{
                    return detail.id === this.state.Postid
                }
            );

            tags = filteretags[0].Ratings.map((detail1 , i) => {
                return (
                    <ListItem key={i}>
                        <Text style={styles.UserComment}> {detail1.name}-  </Text>
                        <Badge style={styles.albadges} key={i}>
                            {detail1.rating}
                        </Badge>
                    </ListItem>
                )
            })

        }


        return (

            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={240}
                drawerPosition={DrawerLayoutAndroid.positions.Right}
                renderNavigationView={() => navigationView}>

                <ScrollView
                    showsVerticalScrollIndicator ={true}
                    onScroll={this.handleScroll.bind(this)}
                    style={styles.overcontainer}>

                    <View style={styles.Filterbar}>
                    <Buttons onPress={this.openDrawer.bind(this)}>
                        <Image source={require('../../../Images/filter2.png')}/>
                        <Text style={styles.Filter}>  Filter </Text>
                    </Buttons>
                    </View>
                    {ListShow}

                </ScrollView>

                <PopupDialog
                        ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                        width= {320}
                        height={350}>

                        <ScrollView style={styles.alltags}>
                            {tags}
                        </ScrollView>

                </PopupDialog>

                <PopupDialog
                    ref={ (ClosetagDialog) => { this.ClosetagDialog = ClosetagDialog; }}
                    width= {320}
                    height={190}>

                        <OpenandCloseIssuePopup
                            CloseIssueId={this.state.CloseIssueId}
                            closeIssueclosepopup={this.closeIssueclosepopup.bind(this)}
                            closureNoteValue={this.state.closureNoteValue}
                            ShowingValuesLocally={this.ShowingValuesLocally.bind(this)}
                        />

                </PopupDialog>

                <PopupDialog
                    ref={ (OpenIssuetagDialog) => { this.OpenIssuetagDialog = OpenIssuetagDialog; }}
                    width= {320}
                    height={150}>

                    <Openissue
                        CloseIssueId={this.state.CloseIssueId}
                        closeIssueopenpopup={this.closeIssueopenpopup.bind(this)}
                        OpenLocallyIssue={this.OpenLocallyIssue.bind(this)}
                    />

                </PopupDialog>

            </DrawerLayoutAndroid>

        );
    }
}

const styles = StyleSheet.create({

    overcontainer: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.2)',
    },

    Filterbar:{
        marginRight: 10,
        marginBottom:2,
        alignItems:'flex-end',
    },

    Filter:{
        fontSize:16,
        color:"black",
        alignItems:'flex-end',
    },

    arrowstyle:{
        marginTop:14,
        marginLeft:8,
    },

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    albadges:{
        backgroundColor:'rgba(102,178,255,1)',
    },

    Discuss:{
        flexWrap:"wrap",
        paddingLeft:20,
        paddingRight:20,
    },

    inputtags:{
        marginBottom:20,
        marginTop:20,
        marginLeft: 10,
    },

    filterbackground:{
        backgroundColor:"rgba(2,59,79,0.0)",
    },

    FilterTextColor:{
        color:"#fff"
    },

    loading:{
        alignItems:"center",
        color:"#fff",
        fontSize:15,
    },

    ViewLoading:{
        flex:1,
        alignItems:"center",
        // backgroundColor:"rgba(0, 0, 0, 0.3)",
        height:35,
        justifyContent: 'center',
    },

    locationiconsize:{
        fontSize:15,
        color:"#66CCCC"
    },

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },

    CheckboxText:{
        marginLeft:6,
    },

    closetagsbutton:{
        flexDirection: 'row',


    },

    ButtonStyles:{
        backgroundColor:'#66CCCC',
        color:'#000',
        flex:0.5,
        marginLeft:9,
        marginRight:9,
    },

    closetagpopup:{
        alignItems: 'center',
        justifyContent: 'center',
    },

    Imagestyle:{
        marginTop:1,
        marginBottom:2,
        opacity:0.8,
    },

    fadebox:{
        opacity:0.8,
    },


})


export default connect(Issues => ({
        Issues: Issues.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(IssuesWidgit);
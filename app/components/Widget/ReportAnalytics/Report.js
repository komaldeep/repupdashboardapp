import React, {Component} from 'react';
import { Container, Content, List, ListItem, Icon} from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, Image, ScrollView, Alert, Text, Picker, Item, Spinner, ListView,
    TextInput,
    ART,
    LayoutAnimation} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import myTheme from '../../../Theme/Theme';
var Realm = require('realm');
import Buttons from 'react-native-button';
import Dimensions from 'Dimensions';
import Button from 'apsl-react-native-button';
import { Bar, Pie } from 'react-native-pathjs-charts'
const {
    Surface,
    Group,
    Rectangle,
    Shape,
} = ART;

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height - 25,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height-25,
};

class Report extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            clicked: false,
            selected1: 'key1',
            language:'',
            realm:'',
            visible: true,
            loading:6,
            Postid:'',
            MoreList:false,
            selected:true
        };
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });
        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);
        this.setState({
            AuthHeader:Authobjects,
        });

    }

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    closeDrawer(){
        this.setState({
            clicked: true,
            MoreList: false,
        })
    }

    openPopupBox(event){

        this.setState({
            PostTags: event
        })
        this.popupDialog.openDialog();

    }

    handleScroll(event){

        let windowHeight = Dimensions.get('window').height,
            height = event.nativeEvent.contentSize.height,
            offset = event.nativeEvent.contentOffset.y;

        if( windowHeight + offset >= height ){
            let loadingnumber = this.state.loading;
            let addingnumber = 6;
            this.setState({
                loading: loadingnumber+ addingnumber,
            })
            this.LoadMoreData();
        }

    }




    GetData(){

        let replies = ["all"];
        let category_mentions= ["all"];
        let date_range = this.state.Month;
        let nps="all";
        let properties = this.state.Properties;
        let sources = this.state.Overallproperties;
        let tags = [];
        let limit = 6;
        let rating = -1;
        let skip = 0;

        this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

    }



    render() {

        const { loader, ReviewsFeed, propertieslist, agodalist , bookinglist, goibibolist } = this.props.state;

        let properties, OtaList;
        let Propertyarray = [];


        let showdata, realobjectlength, ReviewsFeedarray, filteretags ,tags;



        let data = [{
            "name": "Washington",
            "population": 7694980
        }, {
            "name": "Oregon",
            "population": 2584160
        }, {
            "name": "Minnesota",
            "population": 6590667
        }, {
            "name": "Alaska",
            "population": 7284698
        }]

        let options = {
            margin: {
                top: 20,
                left: 20,
                // right: 20,
                // bottom: 20
            },
            width: 350,
            height: 350,
            color: '#2980B9',
            r: 50,
            R: 150,
            legendPosition: 'topLeft',
            animate: {
                type: 'oneByOne',
                duration: 200,
                fillTransition: 3
            },
            label: {
                fontFamily: 'Arial',
                fontSize: 8,
                fontWeight: true,
                color: '#ECF0F1'
            }
        }

        let data1 = [
            {"number":  8, "name": 'Fun activities'},
            {"number": 7, "name": 'Dog'},
            {"number": 16, "name": 'Food'},
            {"number": 23, "name": 'Car'},
            {"number": 42, "name": 'Rent'},
            {"number":  4, "name": 'Misc'},
        ];

        return (
           <ScrollView style={styles.overcontainer}>
               <View style={styles.Filterbar}>
                   <Buttons >
                       <Image source={require('../../../Images/filter2.png')}/>
                       <Text style={styles.Filter}>  Filter </Text>
                   </Buttons>
               </View>
               <Text> Joins </Text>

               <Pie
                   data={data}
                   options={options}
                   accessorKey="population" />

           </ScrollView>
        );

    }
}

const styles = StyleSheet.create({

    overcontainer: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.2)',
    },

    Filterbar:{
        marginRight: 10,
        marginBottom:2,
        alignItems:'flex-end',
        marginTop:4,
    },

    fadebox:{
        opacity:0.8,
    },

    inputgroupstyle:{
        flex:1,
        fontSize:11,
        // marginTop: 10,
        borderBottomColor: 'black',
        borderBottomWidth: .5,
    },

    createissuesview:{

        marginLeft:9,
        marginRight:15,
        marginTop:30,
    },

    Filter:{
        fontSize:16,
        color:"black",
        alignItems:'flex-end',
    },

    Drawerhead:{
        fontSize:16,
    },

    arrowstyle:{
        marginTop:14,
        marginLeft:8,
    },

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    // albadges:{
    //     backgroundColor:'rgba(102,204,204,1)',
    // },

    loading:{
        alignItems:"center",
        color:"#fff",
        fontSize:15,
    },

    ViewLoading:{
        flex:1,
        alignItems:"center",
        // backgroundColor:"rgba(0, 0, 0, 0.3)",
        // height:30,
        justifyContent: 'center',
        marginTop:5,
        flexDirection:"row",
    },

    Noreviewavaialble:{
        flex:1,
        alignItems:"center",
        justifyContent: 'center',
        marginTop:10,
        flexDirection:"row",
    },

    locationiconsize:{
        fontSize:15,
        color:"#66CCCC"
    },

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },

    CheckboxText:{
        marginLeft:6,
        // marginBottom:10,
    },

    Imagestyle:{
        marginTop:1,
        marginBottom:2,
        opacity:0.8,
    },

    albadges1:{
        backgroundColor:'rgba(102,204,204,0.99)'
    },

    albadges:{
        backgroundColor:'rgba(102,204,204,0.0)'
    },

    BadgeText:{
        // flex:1,
        fontSize:12,
        justifyContent: 'flex-end',
        color:"#000000",
        fontWeight:"bold",
        // alignItems:"flex-end",
    },

    BadgeView:{
        flex:1,

    }

})

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Report);

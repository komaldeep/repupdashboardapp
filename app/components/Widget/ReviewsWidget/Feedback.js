import React, {Component} from 'react';
import { Container, Content, List, ListItem, Icon} from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, Image, ScrollView, Alert, Text, Picker, Item, Spinner, ListView, TextInput } from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import ReviewsDetail from './ReviewsDetail';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import myTheme from '../../../Theme/Theme';
import PopupDialog, { DialogTitle } from 'react-native-popup-dialog';
import Badge from 'react-native-smart-badge';
import PropertyName from '../../Drawer/Feedback/Propertyname';
var Realm = require('realm');
import Buttons from 'react-native-button';
import Dimensions from 'Dimensions';
import Button from 'apsl-react-native-button';
import Rating from '../../Drawer/Feedback/Rating';
import CreateIssuePopup from '../../Drawer/Feedback/CreateIssuePopup';

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height - 25,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height-25,
};

class Feedback extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            clicked: false,
            selected1: 'key1',
            language:'',
            realm:'',
            visible: true,
            loading:6,
            Postid:'',
            MoreList:false,
            selected:true,
            Agoda: false,
            booking:false,
            Goibibo:false,
            Expedia:false,
            HolidayIQ:false,
            Makemytrip:false,
            Tripadviser:false,
            Zomoto:false,
            Tripadviser_restaurant:false,
            Facebook:false,
            Google:false,
            OtpProperties: true,
            postids:'',
            comment:'',
            AuthHeader:'',
            PostTags:'',
            Overallproperties:["all"],
            Properties:["all"],
            Month:{"start":"2017-06-01",
                "end":"2017-06-30"},
            MonthCal:'June2017',
            loadingDataPush:[],
            AllProperties:true,
            MakeOtherproperties:'',
            templatebody:[],
            TemplateText:"",
            ReviewsKey:"",
            Key:'',
            AlllSentiments:true,
            Dectractors:false,
            Promoters:false,
            Passive:false,
            nps:"all",
            AlllReply:true,
            Replied:false,
            NotReplies:false,
            replies:["all"],
            Rating: -1,
            AllRating:true,
            Issueid:'',
            Propertyid:''
        };
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });
        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);
        this.setState({
            AuthHeader:Authobjects,
        });

    }

    openDrawer() {
    this.refs['myDrawer'].openDrawer();
    }

    closeDrawer(){
        this.setState({
            clicked: true,
            MoreList: false,
        })
    }

    openPopupBox(event){

        this.setState({
            PostTags: event
        });
        this.popupDialog.openDialog();

    }

    handleScroll(event){

        let windowHeight = Dimensions.get('window').height,
            height = event.nativeEvent.contentSize.height,
            offset = event.nativeEvent.contentOffset.y;

        if( windowHeight + offset >= height ){
            let loadingnumber = this.state.loading;
            let addingnumber = 6;
            this.setState({
                loading: loadingnumber+ addingnumber,
            })
            this.LoadMoreData();
        }

    }

    LoadMoreData(){

        let replies = this.state.replies;
        let category_mentions= ["all"];
        let date_range = this.state.Month;
        let nps= this.state.nps;
        let properties = this.state.Properties;
        let sources = this.state.Overallproperties;
        let tags = [];
        let limit = this.state.loading;
        let rating = this.state.Rating;
        let skip = 0;

        this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

    }

    Moreabout(){
        this.setState({
            MoreList: true,
        })
    }

    Lessabout(){
        this.setState({
            MoreList: false,
        })
    }

    GetData(){

        let replies = this.state.replies;
        let category_mentions= ["all"];
        let date_range = this.state.Month;
        let nps= this.state.nps;
        let properties = this.state.Properties;
        let sources = this.state.Overallproperties;
        let tags = [];
        let limit = 6;
        let rating = this.state.Rating;
        let skip = 0;

        this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

    }

    Alll(){

        this.setState({
            Overallproperties:["all"],
            selected: !this.state.selected,
            Agoda: false,
            booking:false,
            Goibibo:false,
            Expedia:false,
            HolidayIQ:false,
            Makemytrip:false,
            Tripadviser:false,
            Zomoto:false,
            Tripadviser_restaurant:false,
            Facebook:false,
            Google:false,
        });

        if(this.state.selected == false){
            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = this.state.Month;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = ["all"];
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
        }

        this.refs['myDrawer'].closeDrawer();
    }

    Agoda() {

        if (this.state.Agoda == false) {
            this.state.Overallproperties.push("agoda");

            let idx = this.state.Overallproperties.indexOf("all");
            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            // console.log(this.state.Overallproperties);
            this.setState({
                Overallproperties: this.state.Overallproperties,
                selected: false,
                Agoda: !this.state.Agoda,
            });

            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("agoda");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            // console.log(this.state.Overallproperties);
            this.setState({
                Overallproperties: this.state.Overallproperties,
                selected: false,
                Agoda: !this.state.Agoda,
            });
            this.GetData();
        }
    }

    Booking(){

        this.setState({
            selected: false,
            booking: !this.state.booking,
        });

        if (this.state.booking == false) {
            this.state.Overallproperties.push("booking");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }

            this.setState({
                Overallproperties: this.state.Overallproperties
            });

            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("booking");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }

    }

    Goibibo(){

        this.setState({
            selected: false,
            Goibibo: !this.state.Goibibo,
        });

        if (this.state.Goibibo == false) {
            this.state.Overallproperties.push("goibibo");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("goibibo");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }

    }

    propertynames(propertyid){

            this.state.Properties.push(propertyid);
            let idx = this.state.Properties.indexOf("all");

            if (idx != -1) {
                this.state.Properties.splice(idx, 1);
            }
            this.setState({
                Properties: this.state.Properties,
                AllProperties:false
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();

    }

    ischeckemptyproperties(){

        if(this.state.Properties == []){
            this.state.Properties.push("all");
            this.setState({
                Properties: this.state.Properties
            })
            this.GetData();
        }

    }

    unchecked(event){

        let idx = this.state.Properties.indexOf(event);

        if (idx != -1) {
            this.state.Properties.splice(idx, 1);
        }

        this.setState({
            Properties: this.state.Properties
        });

        this.GetData();
        this.ischeckemptyproperties();

    }

    Expedia(){
        this.setState({
             Expedia: !this.state.Expedia,
            selected: false,
        })
        if (this.state.Expedia == false) {
            this.state.Overallproperties.push("expedia");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("expedia");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    HolidayIQ(){
        this.setState({
            HolidayIQ:!this.state.HolidayIQ,
            selected: false,
        })
        if (this.state.HolidayIQ == false) {
            this.state.Overallproperties.push("holidayiq");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("holidayiq");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Makemytrip(){
        this.setState({
            Makemytrip: !this.state.Makemytrip,
            selected: false,
        });

        if (this.state.Makemytrip == false) {
            this.state.Overallproperties.push("makemytrip");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("makemytrip");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Tripadviser(){
        this.setState({
            Tripadviser: !this.state.Tripadviser,
            selected: false,
        });

        if (this.state.Tripadviser == false) {
            this.state.Overallproperties.push("tripadvisor");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("tripadvisor");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Zomoto(){
        this.setState({
            Zomoto:!this.state.Zomoto,
            selected: false,
        })
        if (this.state.Zomoto == false) {

            this.state.Overallproperties.push("zomato");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();

        }
        else {
            let idx = this.state.Overallproperties.indexOf("zomato");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Tripadviser_restaurant(){
        this.setState({
            Tripadviser_restaurant:!this.state.Tripadviser_restaurant,
            selected: false,
        })

        if (this.state.Tripadviser_restaurant == false) {
            this.state.Overallproperties.push("tripadvisor-restaurant");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();

        }
        else {
            let idx = this.state.Overallproperties.indexOf("tripadvisor-restaurant");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Facebook(){
        this.setState({
            Facebook:!this.state.Facebook,
            selected: false,
        })
        if (this.state.Facebook == false) {
            this.state.Overallproperties.push("facebook");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();

        }
        else {
            let idx = this.state.Overallproperties.indexOf("facebook");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
    }

    Google(){

        this.setState({
            Google:!this.state.Google,
            selected: false,
        })

        if(this.state.Google == false) {
            this.state.Overallproperties.push("google");
            let idx = this.state.Overallproperties.indexOf("all");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
            this.refs['myDrawer'].closeDrawer();
        }
        else {
            let idx = this.state.Overallproperties.indexOf("google");

            if (idx != -1) {
                this.state.Overallproperties.splice(idx, 1);
            }
            this.setState({
                Overallproperties: this.state.Overallproperties
            })
            this.GetData();
        }
        
    }

    Pushreplybox(postids, comment){
        this.setState({
            postids,
            comment
        })

        let body = {postid :postids , Comment :comment}
        // this.props.actions.PostingReviewsFeedbackBYOwner(body)

    }

    Issuepopupbox(){
        this.Issuepopuxbox.openDialog();
    }

    PickerMontherData(lang){

        let MonthValue;

        if(lang == "June2017"){
            MonthValue = {
                "start":"2017-06-01",
                "end":"2017-06-30"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = MonthValue;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();
        }

        else if(lang == "May2017"){
            MonthValue = {
                "start":"2017-05-01",
                "end":"2017-05-30"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = MonthValue;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "April2017"){
            MonthValue = {
                "start":"2017-04-01",
                "end":"2017-04-30"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = MonthValue;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang =="March2017"){

            MonthValue = {
                "start":"2017-03-01",
                "end":"2017-03-31"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = MonthValue;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();

        }
        else if(lang == "Februry2017"){

            MonthValue = {
                "start":"2017-02-01",
                "end":"2017-02-28"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = MonthValue;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "January2017"){

            MonthValue={
                "start":"2017-01-01",
                "end":"2017-01-31"
            };
            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = MonthValue;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "December2016"){

            MonthValue={
                "start":"2016-12-01",
                "end":"2016-12-31"
            };
            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = MonthValue;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "November2016"){

            MonthValue={
                "start":"2016-11-01",
                "end":"2016-11-30"
            };
            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = MonthValue;
            let nps= this.state.nps;
            let properties = this.state.Properties;
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();

        }

    }

    PressAlProperties(){

        if(this.state.AllProperties == false) {
            this.setState({
                Properties: ["all"],
                AllProperties:true,
            });

            let replies = this.state.replies;
            let category_mentions= ["all"];
            let date_range = this.state.Month;
            let nps="all";
            let properties = ["all"];
            let sources = this.state.Overallproperties;
            let tags = [];
            let limit = 6;
            let rating = this.state.Rating;
            let skip = 0;

            this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

        }
        else {
            this.setState({
                AllProperties:false,
            });

        }
        this.refs['myDrawer'].closeDrawer();

    }

    Templetesbox(templatebody,ReviewsKey){

        this.setState({
            templatebody,
            Key:ReviewsKey,
        });

        this.TemplatepopupDialog.openDialog();
    }

    SelectedTemplates(TemplateName){

        // console.log(TemplateName,'TemplateName is');

        this.setState({
            TemplateText:TemplateName,
            ReviewsKey: this.state.Key,
        });

        this.TemplatepopupDialog.closeDialog();

    }

    LoadingdataonRequest(npsdata, replieddata, Rating){

        let replies = replieddata;
        let category_mentions= ["all"];
        let date_range = this.state.Month;
        let nps= npsdata;
        let properties = this.state.Properties;
        let sources = this.state.Overallproperties;
        let tags = [];
        let limit = 6;
        let rating = Rating;
        let skip = 0;

        this.props.actions.ReviewsFeeddata(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

    }

    AlllSentiments(){
        this.setState({
            AlllSentiments:true,
            Dectractors:false,
            Passive:false,
            Promoters:false,
            nps:"all",
        });
        let rating = this.state.Rating;
        let nps = "all";
        let replieddata = this.state.replies;
        this.LoadingdataonRequest(nps, replieddata, rating);
        this.refs['myDrawer'].closeDrawer();
    }

    Dectractors(){
        this.setState({
            Dectractors:true,
            AlllSentiments:false,
            Passive:false,
            Promoters:false,
            nps:-1,
        });
        let nps = -1;
        let rating = this.state.Rating;
        let replieddata = this.state.replies;
        this.LoadingdataonRequest(nps, replieddata, rating);
        this.refs['myDrawer'].closeDrawer();
    }

    Passive(){
        this.setState({
            Dectractors:false,
            AlllSentiments:false,
            Passive:true,
            Promoters:false,
            nps: 0,
        });
        let nps = 0;
        let rating = this.state.Rating;
        let replieddata = this.state.replies;
        this.LoadingdataonRequest(nps, replieddata, rating);
        this.refs['myDrawer'].closeDrawer();

    }

    Promoters(){
        this.setState({
            Dectractors:false,
            AlllSentiments:false,
            Passive:false,
            Promoters:true,
            nps: 1,
        });
        let nps = 1;
        let replieddata = this.state.replies;
        let rating = this.state.Rating;
        this.LoadingdataonRequest(nps, replieddata, rating);
        this.refs['myDrawer'].closeDrawer();
    }

    AlllReply(){

        this.setState({
            AlllReply:true,
            Replied:false,
            NotReplies:false,
            replies:["all"],
        });

        let npsdata = this.state.nps;
        let replieddata = ["all"];
        let rating = this.state.Rating;
        this.LoadingdataonRequest(npsdata, replieddata, rating);
        this.refs['myDrawer'].closeDrawer();

    }

    Replied(){

        this.setState({
            AlllReply:false,
            Replied:true,
            NotReplies:false,
            replies:"replied",
        });

        let npsdata = this.state.nps;
        let replieddata = "replied";
        let rating = this.state.Rating;
        this.LoadingdataonRequest(npsdata, replieddata, rating);
        this.refs['myDrawer'].closeDrawer();

    }

    NotReplies(){

        this.setState({
            AlllReply:false,
            Replied:false,
            NotReplies:true,
            replies:"notreplied",
        });

        let npsdata = this.state.nps;
        let replieddata = "notreplied";
        let rating = this.state.Rating;
        this.LoadingdataonRequest(npsdata, replieddata, rating);
        this.refs['myDrawer'].closeDrawer();

    }

    uncheckedRating(rating){
        console.log(rating, 'rating is');
    }

    Ratingname(Ratingnumber){
        // console.log(Ratingnumber,'Ratingnumber');
        this.setState({
            Rating:Ratingnumber,
            AllRating:false,
        });
        let npsdata = this.state.nps;
        let replieddata = this.state.replies;
        let rating = Ratingnumber;
        this.LoadingdataonRequest(npsdata, replieddata, rating);
    }

    PressallRating(){
        this.setState({
            Rating:-1,
            AllRating:true,
        });
        let rating = -1;
        let npsdata = this.state.nps;
        let replieddata = this.state.replies;
        this.LoadingdataonRequest(npsdata, replieddata, rating);
    }

    CreateIssuepopup(Issueid,Property_id){

        this.setState({
            Issueid,
            Propertyid:Property_id
        });

        this.Issuepopuxbox.openDialog();
    }

    Closeissuepopupbox(close){
        this.Issuepopuxbox.closeDialog();
    }

    render() {

        const { taj, loader, ReviewsFeed, propertieslist, agodalist , bookinglist, goibibolist, profile } = this.props.state;

        console.log( taj ,'taj is');

        let properties, OtaList, alltemplates, profilematch, Sentiments, Reply_status, AllRating;
        let Propertyarray = [];
        let profiles = [];
        if(profile != false) {

            for (let i in profile.profile) {
                profiles.push(profile.profile[i]);
            }
            profilematch = profiles[0]._id;

        }

        // console.log(ReviewsFeed, 'propertieslist');

        if(propertieslist ==  false){
            properties =
                <ListItem>
                  <Text> Loading ... </Text>
                </ListItem>
        }
        else {

            let property_ref;
            for (let i in propertieslist.Properties.data) {
                Propertyarray.push(propertieslist.Properties.data[i]);
            }

                properties =
                <List>
                <ListItem>

                    {
                        this.state.AllProperties ?
                        <TouchableOpacity style={styles.FilterTouch} onPress={this.PressAlProperties.bind(this)}>
                            <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                            <Text style={styles.CheckboxText}> All </Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style={styles.FilterTouch} onPress={this.PressAlProperties.bind(this)}>
                            <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                            <Text style={styles.CheckboxText}> All </Text>
                        </TouchableOpacity>
                    }

                  </ListItem>

                    {
                        Propertyarray.map((detail, i) => {
                            return (
                                <PropertyName
                                    ref={i}
                                    key={i}
                                    item={detail}
                                    propertyname={this.propertynames.bind(this)}
                                    uncheckedpropertyname={this.unchecked.bind(this)}
                                />
                            )
                        })
                    }

                </List>
        }

        //popupbox with templates
        if(this.state.templatebody.length == 0 ){
            alltemplates =  <List>
                <ListItem>
                    <Text>
                       Sorry, No Template is available for this Property
                    </Text>
                </ListItem>
            </List>
        }
        else {
            alltemplates = <List>
                {
                    this.state.templatebody.map(
                        (detail , i) => {
                            return(
                                <ListItem key={i}>

                                    <Buttons onPress={this.SelectedTemplates.bind( this, detail.body)} key={i}>
                                            <Text>
                                            {detail.title}
                                        </Text>
                                    </Buttons>

                                </ListItem>
                            )
                        }
                    )
                }
            </List>
        }


        if(ReviewsFeed != false){

           // let metatags = ReviewsFeed.reviewsfeeddata.meta.ota_counts;

            if (this.state.MoreList == false) {
                OtaList =
                    <List>
                        <ListItem itemDivider>
                            <Text> Sources </Text>
                        </ListItem>

                        <ListItem>
                            {this.state.selected ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Alll.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Alll.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                            }
                        </ListItem>

                        <ListItem>
                            {this.state.Agoda ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Agoda.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Agoda </Text>

                                    <View style={styles.BadgeView}>

                                    </View>

                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Agoda.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Agoda </Text>


                                </TouchableOpacity>
                            }
                        </ListItem>

                        <ListItem>
                            {this.state.booking ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Booking.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Booking </Text>

                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Booking.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Booking</Text>

                                </TouchableOpacity>
                            }
                        </ListItem>

                        <ListItem>
                            {this.state.Goibibo ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Goibibo.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Goibibo </Text>
                                </TouchableOpacity>

                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Goibibo.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Goibibo </Text>

                                </TouchableOpacity>
                            }
                        </ListItem>

                        <ListItem>
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Moreabout.bind(this)}>
                                <Text> More... </Text>
                            </TouchableOpacity>
                        </ListItem>
                    </List>
            }
            else {
                OtaList =
                    <List>
                        <ListItem itemDivider>
                            <Text> Sources </Text>
                        </ListItem>

                        <ListItem>

                            {this.state.selected ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Alll.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Alll.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>


                            {this.state.Agoda ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Agoda.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Agoda </Text>


                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Agoda.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Agoda </Text>

                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>

                            {this.state.booking ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Booking.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Booking </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Booking.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Booking</Text>

                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>

                            {this.state.Goibibo ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Goibibo.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Goibibo </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Goibibo.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Goibibo </Text>

                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>

                            {this.state.Expedia ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Expedia.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Expedia </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Expedia.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Expedia </Text>


                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>

                            {this.state.HolidayIQ ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.HolidayIQ.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> HolidayIQ </Text>

                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.HolidayIQ.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> HolidayIQ </Text>

                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>

                            {this.state.Makemytrip ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Makemytrip.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Makemytrip </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Makemytrip.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Makemytrip </Text>

                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>
                            {this.state.Tripadviser ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Tripadviser.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Tripadviser </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Tripadviser.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}>Tripadviser </Text>

                                </TouchableOpacity>
                            }
                        </ListItem>

                        <ListItem>
                            {this.state.Zomoto ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Zomoto.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Zomoto </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Zomoto.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Zomoto </Text>

                                </TouchableOpacity>}
                        </ListItem>

                        <ListItem>


                            {this.state.Tripadviser_restaurant ?
                                <TouchableOpacity style={styles.FilterTouch}
                                                  onPress={this.Tripadviser_restaurant.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Tripadviser restaurant </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch}
                                                  onPress={this.Tripadviser_restaurant.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Tripadviser restaurant </Text>

                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>
                            {this.state.Facebook ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Facebook.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Facebook </Text>

                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Facebook.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Facebook </Text>

                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>

                            {this.state.Google ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Google.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> Google </Text>

                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.Google.bind(this)}>
                                    <Image style={styles.Imagestyle}
                                           source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> Google </Text>

                                </TouchableOpacity>
                            }

                        </ListItem>

                        <ListItem>
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Lessabout.bind(this)}>
                                <Text> Less... </Text>
                            </TouchableOpacity>
                        </ListItem>

                    </List>
            }

            Sentiments =
                <List>
                    <ListItem itemDivider>
                        <Text> Sentiments </Text>
                    </ListItem>

                    <ListItem>
                        {this.state.AlllSentiments ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.AlllSentiments.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.AlllSentiments.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.Dectractors ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Dectractors.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Detractors </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Dectractors.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Detractors </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.Passive ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Passive.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Passive </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Passive.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Passive </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.Promoters ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Promoters.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Promoters </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Promoters.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Promoters </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>
                </List>

            Reply_status =
                <List>
                    <ListItem itemDivider>
                        <Text> Reply Status </Text>
                    </ListItem>

                    <ListItem>
                        {this.state.AlllReply ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.AlllReply.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.AlllReply.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.Replied ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Replied.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Replied </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Replied.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Replied </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.NotReplies ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.NotReplies.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Not Replied </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.NotReplies.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Not Replied </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                </List>


            let Stars = [1, 2, 3,4,5,6,7,8,9,10];
            AllRating =
                <List>
                    <ListItem itemDivider>
                        <Text> Rating </Text>
                    </ListItem>

                    <ListItem>
                        {
                            this.state.AllRating ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.PressallRating.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.PressallRating.bind(this)}>
                                    <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                        }

                    </ListItem>

                    {
                        Stars.map((detail, i) => {
                            return (
                                <Rating
                                    ref={i}
                                    key={i}
                                    item={detail}
                                    Ratingname={this.Ratingname.bind(this)}
                                    uncheckedRating={this.uncheckedRating.bind(this)}
                                />
                            )
                        })
                    }

                </List>



        }

        let navigationView = (
            <Container>
                <Content theme={myTheme}>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Filters </Text>
                    </View>

                    <List>
                        <ListItem>
                            <Picker
                                selectedValue={this.state.MonthCal}
                                onValueChange={this.PickerMontherData.bind(this)}>

                                <Picker.Item label="June 2017" value="June2017" />
                                <Picker.Item label="May 2017" value="May2017" />
                                <Picker.Item label="April 2017" value="April2017" />
                                <Picker.Item label="March 2017" value="March2017" />
                                <Picker.Item label="February 2017" value="Februry2017" />
                                <Picker.Item label="January 2017" value="January2017" />
                                <Picker.Item label="December 2016" value="December2016" />
                                <Picker.Item label="November 2016" value="November2016" />

                            </Picker>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text> Properties </Text>
                        </ListItem>

                        {properties}

                        {OtaList}

                        {Sentiments}

                        {Reply_status}

                        {AllRating}

                    </List>
                </Content>
            </Container>
        );

        var realm = new Realm({
            schema: [{name: 'Feedbackdata', properties: {feedbackappdata: 'string'}}]
        });

        let showdata, realobjectlength, ReviewsFeedarray, filteretags ,tags;

            if (ReviewsFeed == false) {

                let backupdata = realm.objects('Feedbackdata');

                if (realm.objects('Feedbackdata').length == 1) {

                    let stringtoarray = backupdata[0].feedbackappdata;

                    let feedbackobjects = JSON.parse(stringtoarray);

                    ReviewsFeedarray = [];
                    for (let i in feedbackobjects) {
                        ReviewsFeedarray.push(feedbackobjects[i]);
                    }

                    showdata =
                        ReviewsFeedarray.map((detail, i) => {
                            return (
                                <ReviewsDetail
                                               OpenissuePopupbox={this.Issuepopupbox.bind(this)}
                                               PushReviewReply={this.Pushreplybox.bind(this)}
                                               openpopupbox={this.openPopupBox.bind(this)}
                                               navigator={this.props.navigator}
                                               detail={detail}
                                               Propertyarray = {Propertyarray}
                                               key={detail._id}
                                               ReviewFiedValue={detail._id}
                                               openTemplatebox={this.Templetesbox.bind(this)}
                                               TemplateText={this.state.TemplateText}
                                               ReviewsKey={this.state.ReviewsKey}
                                               profilematch={profilematch}
                                               profiles={profiles}
                                               opencreateissuepopup={this.CreateIssuepopup.bind(this)}
                                />
                            )
                    })
                }
            }
            else {
                let stringyfyobject = JSON.stringify(ReviewsFeed.reviewsfeeddata.data);

                 realm.write(() => {
                    let backupdata = realm.create('Feedbackdata', {feedbackappdata: stringyfyobject});
                  });

                let objectlength = realm.objects('Feedbackdata').length;
                if (objectlength > 1) {
                    realm.write(() => {
                        let backupdata = realm.create('Feedbackdata', {feedbackappdata: stringyfyobject});
                        let allobjects = realm.objects('Feedbackdata');
                        realm.delete(allobjects);
                        realm.create('Feedbackdata', {feedbackappdata: stringyfyobject});
                    });
                }


                ReviewsFeedarray = [];
                for (let i in ReviewsFeed.reviewsfeeddata.data) {
                    ReviewsFeedarray.push(ReviewsFeed.reviewsfeeddata.data[i]);
                }

                if (ReviewsFeedarray.length == 0) {
                    showdata = <View style={styles.ViewLoading}>
                        <Text> Sorry, No Review is available  </Text>
                    </View>
                }
                else {
                    showdata =
                        ReviewsFeedarray.map((detail, i) => {
                            return (
                                <ReviewsDetail
                                    OpenissuePopupbox={this.Issuepopupbox.bind(this)}
                                    PushReviewReply={this.Pushreplybox.bind(this)}
                                    openpopupbox={this.openPopupBox.bind(this)}
                                    navigator={this.props.navigator}
                                    Propertyarray = {Propertyarray}
                                    detail={detail}
                                    k key={detail._id}
                                    ReviewFiedValue={detail._id}
                                    openTemplatebox={this.Templetesbox.bind(this)}
                                    TemplateText={this.state.TemplateText}
                                    ReviewsKey={this.state.ReviewsKey}
                                    profilematch={profilematch}
                                    profiles={profiles}
                                    opencreateissuepopup={this.CreateIssuepopup.bind(this)}
                                    />
                            )
                        })
                }
            }


        if(this.state.PostTags == ''){
            tags = <View></View>
        }
        else {

            let alltags = [];
            let alltagsheader = [];
            for (let i in this.state.PostTags) {
                if (this.state.PostTags.hasOwnProperty(i)) {
                    alltags.push([i, this.state.PostTags[i]])
                }
            }

            tags = alltags.map((detail, i) =>{
                    return(
                        <List key ={i}>
                            <ListItem key ={i}>
                                <Text> {detail[0]}  </Text>
                                <Badge key ={i} style={styles.albadges1}>
                                     {detail[1]}
                                </Badge>
                            </ListItem>
                        </List>
                    )
            })

        }


        return (

            <DrawerLayoutAndroid
                                 ref="myDrawer"
                                 drawerWidth={240}
                                 drawerPosition={DrawerLayoutAndroid.positions.Right}
                                 renderNavigationView={() => navigationView}>

                <ScrollView
                    showsVerticalScrollIndicator ={true}
                    onScroll={this.handleScroll.bind(this)}
                    style={styles.overcontainer}>

                     <View style={styles.Filterbar}>
                        <Buttons onPress={this.openDrawer.bind(this)}>
                            <Image source={require('../../../Images/filter2.png')}/>
                            <Text style={styles.Filter}>  Filter </Text>
                        </Buttons>
                     </View>

                     {showdata}

                </ScrollView>

                <PopupDialog
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    width= {320}
                    height={350}>

                    <ScrollView style={styles.alltags}>
                        {tags}
                    </ScrollView>

                </PopupDialog>

                <PopupDialog
                    ref={(TemplatepopupDialog) => { this.TemplatepopupDialog = TemplatepopupDialog; }}
                    width= {320}
                    height={250}>

                    <ScrollView style={styles.alltags}>
                        {alltemplates}
                    </ScrollView>

                </PopupDialog>

                <PopupDialog
                    dialogTitle={<DialogTitle title="Create Issue" />}
                    ref={(Issuepopuxbox) => { this.Issuepopuxbox = Issuepopuxbox; }}
                    width= {320}
                    height={480}>

                    <CreateIssuePopup
                        Issueid={this.state.Issueid}
                        Propertyid={this.state.Propertyid}
                        Propertyarray={Propertyarray}
                        Source="review"
                        Closepopupbox={this.Closeissuepopupbox.bind(this)}

                    />

                </PopupDialog>

            </DrawerLayoutAndroid>

        );
    }
}

const styles = StyleSheet.create({

    overcontainer: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.2)',
    },

    Filterbar:{
        marginRight: 10,
        marginBottom:2,
        alignItems:'flex-end',
        marginTop:4,
    },

    fadebox:{
      opacity:0.8,
    },

    NotAvailable:{
        color:"#000"
    },

    inputgroupstyle:{
        flex:1,
        fontSize:11,
        // marginTop: 10,
        borderBottomColor: 'black',
        borderBottomWidth: .5,
    },

    createissuesview:{

        marginLeft:9,
        marginRight:15,
        marginTop:30,
    },

    Filter:{
        fontSize:16,
        color:"black",
        alignItems:'flex-end',
    },

    Drawerhead:{
        fontSize:16,
    },

    arrowstyle:{
        marginTop:14,
        marginLeft:8,
    },

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    // albadges:{
    //     backgroundColor:'rgba(102,204,204,1)',
    // },

    loading:{
        alignItems:"center",
        color:"#fff",
        fontSize:15,
    },

    ViewLoading:{
        flex:1,
        alignItems:"center",
        // backgroundColor:"rgba(0, 0, 0, 0.3)",
        // height:30,
        justifyContent: 'center',
        marginTop:5,
        flexDirection:"row",
    },

    Noreviewavaialble:{
        flex:1,
        alignItems:"center",
        justifyContent: 'center',
        marginTop:10,
        flexDirection:"row",
    },

    locationiconsize:{
        fontSize:15,
        color:"#66CCCC"
    },

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },

    CheckboxText:{
        marginLeft:6,
        // marginBottom:10,
    },

    Imagestyle:{
        marginTop:1,
        marginBottom:2,
        opacity:0.8,
    },

    albadges1:{
        backgroundColor:'rgba(102,204,204,0.99)'
    },

    albadges:{
        backgroundColor:'rgba(102,204,204,0.0)'
    },

    BadgeText:{
        // flex:1,
        fontSize:12,
        justifyContent: 'flex-end',
        color:"#000000",
        fontWeight:"bold",
        // alignItems:"flex-end",
    },

    BadgeView:{
        flex:1,

    }

})

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Feedback);



import React, {Component} from 'react';
import { Container, Content, Card, CardItem, Icon, Button, InputGroup, Input } from 'native-base';
import {StyleSheet, View, Navigator, TouchableOpacity, TouchableHighlight, Image, ScrollView, Alert, Modal, Text, TextInput} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';
import CreateIssue from './../../../containers/CreateIssue';
import PopupDialog from 'react-native-popup-dialog';
var moment = require('moment');
const Dimensions = require('Dimensions');
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height,
};

class ReviewsDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            Count:'0',
            text:'',
            SubmitReply:'',
            reply:false,
            isreplible:true,
            profileid:'',
            AuthHeader:'',
            Replybutton: false,
            Credentials:false,
         //   temptext:'0RT @AnOriginalCopy: @RoflGandhi_ Forbes \" India is No.1...\"\nBhakt \" desh badal raha hai \"\nForbes \" in corruption\"\nBhakt \" mandir wahin bane\u2026'
        };
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });
        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);
        this.setState({
            AuthHeader:Authobjects,
        });

    }


    reply(){

        let Sourcename = this.props.detail.source;

        let Propertiesarray = this.props.Propertyarray;
        let Propertyname, k;


        if(Propertiesarray.length != 0) {

            let filteredproperty = Propertiesarray.filter(
                (detail) => {
                    return detail._id == this.props.detail.property_id
                }
            )

            if (filteredproperty != undefined || filteredproperty.length != 0) {
                console.log(filteredproperty, 'filteredproperty 2');
                Propertyname = filteredproperty[0].name;
            }

            let filteredPropertyarray = Propertiesarray.filter(
                (detail) => {
                    return detail.name == Propertyname
                }
            )

            let OtpFeedarray = [];
            for (k in filteredproperty[0].ota_credentials) {
                if(k == Sourcename) {
                    OtpFeedarray.push(filteredproperty[0].ota_credentials[k]);
                }
            }

            // console.log(OtpFeedarray, 'ReviewsFeedarray');

            if(OtpFeedarray[0] == undefined || OtpFeedarray[0].verified == false || OtpFeedarray[0].username == '' || OtpFeedarray[0].username == undefined || OtpFeedarray[0].password == '' || OtpFeedarray[0].password == undefined){
                this.setState({
                    Credentials:true,
                })
            }
            else {
                this.setState({
                    ShowReply: !this.state.ShowReply,
                });
            }

        }

    }

    createissue(){
        this.props.OpenissuePopupbox(this.props.detail._id)
    }

    popupbox(){
        this.props.openpopupbox(this.props.detail.review_data.rating.breakup);
    }

    SubmitComment(){

        let Profile_id = this.props.profilematch;
        let ReplyText = this.state.text;
        let Review_id = this.props.detail._id;
        let AuthHeader = this.state.AuthHeader;

        this.setState({
            ShowReply:false,
            reply:true,
            isreplible:false,
        });

       this.props.actions.PushReviewReply(AuthHeader, Profile_id ,ReplyText ,Review_id);

    }

    showTemplates(){

        let Propertiesarray = this.props.Propertyarray;
        let ReplyTemplates;


        if(Propertiesarray.length != 0) {

            let filteredproperty = Propertiesarray.filter(
                (detail) => {
                    return detail._id == this.props.detail.property_id
                }
            )

            // console.log(filteredproperty, 'filteredproperty');

            if (filteredproperty != undefined) {
                if(filteredproperty[0].reply_templates == undefined || filteredproperty[0].reply_templates == []) {
                    ReplyTemplates = [];
                }
                else {
                    ReplyTemplates = filteredproperty[0].reply_templates;
                }
            }
        }

        // console.log(ReplyTemplates,'ReplyTemplates');

        this.props.openTemplatebox(ReplyTemplates,this.props.ReviewFiedValue);

    }

    componentWillReceiveProps(props){

        if(props.ReviewsKey == this.props.ReviewFiedValue){
            if(props.TemplateText != ''){
                this.setState({
                    text: props.TemplateText
                });
            }
        }

        // console.log(props.detail.review_reply,'props.detail.review_reply.status');

        if(props.detail.review_reply == undefined) {

        }
        else {
            if (props.detail.review_reply.status == "saved_as_draft") {
                this.setState({
                    text: props.detail.review_reply.text
                });
            }
        }
        
    }

    SaveAsDraft(){

        let Profile_id = this.props.profilematch;
        let ReplyText = this.state.text;
        let Review_id = this.props.detail._id;
        let AuthHeader = this.state.AuthHeader;

        this.props.actions.PushReviewReplyDraft(AuthHeader, Profile_id ,ReplyText ,Review_id);
    }

    CheckReplyButton_Show(){

        let Allowed_Otp_Sources = ["booking","tripadvisor",
            "tripadvisor-attraction","holidayiq","goibibo","feedback","google"];

        let mySet = new Set(Allowed_Otp_Sources);
        let OtpExist = mySet.has(this.props.detail.source); // true

        let Replybutton;

        if (this.state.isreplible == false || OtpExist == false && this.props.detail.is_repliable == true || OtpExist == true && this.props.detail.is_repliable == false) {
            Replybutton = false;

        }
        else if (OtpExist == true && this.props.detail.is_repliable == true && this.props.detail.review_reply == undefined) {
            Replybutton = true
        }
        else {
            Replybutton = false
        }

        if(this.props.detail.review_reply != undefined) {
            if (this.props.detail.review_reply.status == "saved_as_draft" && this.state.isreplible == true) {
                Replybutton = true;
            }
        }

        return Replybutton;

    }

    CreateIssuepopup(Propertyname){

          let filteredproperty = this.props.Propertyarray.filter(
                (detail) => {
                    return detail.name == Propertyname
                }
            )
          // console.log(filteredproperty[0]._id, 'Propertyname');

        let Property_id = filteredproperty[0]._id;
        this.props.opencreateissuepopup(this.props.detail._id , Property_id);

    }

    Twitter_profile(){
        if(this.props.detail.user.profile_image_url !=  '' ){
            return (
                <View style={styles.ImageTwitter}>
                    <Image
                        style= {{ height:40, width: 45 }}
                        source={{uri: this.props.detail.user.profile_image_url}}
                    />
                </View>
            )
        }
        else {
            return (
             <View style={styles.ImageTwitter}>
                <Image
                    style= {{ height:40, width: 45 }}
                    source={require('../../../Images/no_profile.png')}
                />
             </View>
            )
        }
    }

    date_converter(){
        let dateString = this.props.detail.tweeted_date;
        let date =   moment(dateString).format('DD/MM/YYYY');
        return date;
    }


    render() {

        let userlocation, PartialRating, REply, Propertyname, REviewUsername, ReplyTemplates;

        // when review is not twitter
        if(this.props.detail.source != 'twitter') {
            if(this.props.detail.reviewer.name != '' && this.props.detail.reviewer.location == ''){
                REviewUsername =
                    <View style={styles.nameandrating}>
                        <Text style={styles.username}>
                            <Icon style={styles.locationiconsize} name='md-contacts'/>   {this.props.detail.reviewer.name}
                        </Text>
                    </View>
            }

            else if(this.props.detail.reviewer.name == '' && this.props.detail.reviewer.location != ''){
                REviewUsername = <View style={styles.nameandrating}>
                    <Text style={styles.username}>
                        <Icon style={styles.locationiconsize} name='ios-pin'/> {this.props.detail.reviewer.location}
                    </Text>
                </View>
            }

            else if(this.props.detail.reviewer.name != '' && this.props.detail.reviewer.location != ''){
                REviewUsername = <View style={styles.nameandrating}>
                    <Text style={styles.username}> <Icon style={styles.locationiconsize} name='md-contacts'/>   {this.props.detail.reviewer.name} | <Icon style={styles.locationiconsize} name='ios-pin'/> {this.props.detail.reviewer.location}
                    </Text>
                </View>
            }

            else {
                REviewUsername = <View></View>
            }


            let dateString = this.props.detail.review_datetime;
            let date =   moment(dateString).format('DD/MM/YYYY');


            if(this.props.detail.review_data.rating.breakup == undefined ){
                PartialRating = true;
            }
            else {
                PartialRating = false;
            }

            let REplyupward = false;

            if(this.state.reply == true){
                REplyupward = true
            }
            else if(this.props.detail.review_reply == undefined || this.props.detail.review_reply == ''){
                REply = false
            }
            else {
                if(this.props.detail.review_reply.status == "replied" || this.props.detail.review_reply.status == "processing" ) {
                    REply = true
                }
                else {
                    REply = false
                }
            }

            let Totalrating;
            if(this.props.detail.nps == -1){
                Totalrating =  <View style={styles.ImageColumnred}>
                    <Text style={styles.OverallRating}>
                        {this.props.detail.review_data.rating.rating}/{this.props.detail.review_data.rating.max}
                    </Text>
                </View>
            }
            else if(this.props.detail.nps == 1){
                Totalrating =  <View style={styles.ImageColumn}>
                    <Text style={styles.OverallRating}>
                        {this.props.detail.review_data.rating.rating}/{this.props.detail.review_data.rating.max}
                    </Text>
                </View>
            }
            else {
                Totalrating =  <View style={styles.ImageColumnblue}>
                    <Text style={styles.OverallRating}>
                        {this.props.detail.review_data.rating.rating}/{this.props.detail.review_data.rating.max}
                    </Text>
                </View>
            }

            let Propertiesarray = this.props.Propertyarray;
            let Hotelname, filteredproperty;
            let ShowCreateIssuebutton = false;


            // console.log(Propertiesarray, 'Propertiesarray');
            if(Propertiesarray.length != 0 ) {

                ShowCreateIssuebutton = true;
                filteredproperty = []

                Propertiesarray.filter(
                    (detail) => {
                        console.log(detail._id == this.props.detail.property_id)
                        if(detail._id == this.props.detail.property_id){
                            filteredproperty.push(detail._id)
                        }
                    }
                )

               // console.log(this.props.detail.property_id,'this.props.detail.property_id')
                //console.log(filteredproperty,'filtered property is')

                if (filteredproperty.length != 0) {

                   // console.log(filteredproperty,filteredproperty.length, 'filteredproperty 1');

                    Propertyname = filteredproperty[0].name;
                    ReplyTemplates = filteredproperty[0].reply_templates;
                }

                Hotelname = <Text style={styles.username}>
                    <Icon style={styles.locationiconsize} name='ios-home'/>   {Propertyname}
                </Text>

            }


            let checkfunctionreturnvalue = false;

            if(this.props.profiles.length != 0) {
                if(this.props.profiles[0].is_admin == true) {
                    checkfunctionreturnvalue = this.CheckReplyButton_Show();
                }
                else {
                    if(this.props.profiles[0].privileges.length != 0){
                        let Checkpost_reply = new Set(this.props.profiles[0].privileges);
                        let post_replyExist = Checkpost_reply.has("post_reply"); // true
                        if(post_replyExist == true){
                            checkfunctionreturnvalue = this.CheckReplyButton_Show();
                        }
                        else if(this.props.profiles[0].privileges.length == 0){
                            checkfunctionreturnvalue = this.CheckReplyButton_Show();
                        }
                    }
                }
            }


            if(this.props.detail.review_reply != undefined) {
                checkfunctionreturnvalue = this.CheckReplyButton_Show();
            }

            let ShowFooter;
            if(PartialRating == false || checkfunctionreturnvalue == true  || ShowCreateIssuebutton == true){
                ShowFooter = true
            }
            else {
                ShowFooter = false
            }
            return (
                <KeyboardAwareScrollView>
                    <Content theme={myTheme}>
                        <Card style={styles.FeedbackRow}>

                            <CardItem>
                                <View style={styles.contentHeader}>
                                    {Totalrating}
                                    <View style={styles.DetaisHeader}>
                                        <Text style={styles.titleofotp}>
                                            {this.props.detail.review_data.title}
                                        </Text>
                                        {REviewUsername}
                                        <Text style={styles.username}>
                                            <Icon style={styles.locationiconsize} name='ios-calendar-outline'/> {date}
                                            | {this.props.detail.source}
                                        </Text>
                                        {Hotelname}
                                    </View>
                                </View>






                                { REplyupward ?
                                    <View style={styles.ReplyTemplete}>
                                        <View style={styles.ReplyTempleteHeader}>
                                            <View style={styles.ImageColumn12}>
                                                <Image source={require('../../../Images/manager2.png')}/>
                                            </View>
                                            <View style={styles.Titleandreply}>
                                                <Text style={styles.titleofotp1}>
                                                    Management Response
                                                </Text>
                                                <Text style={styles.UserComment}>
                                                    {this.state.text}
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                    :
                                    <View></View>
                                }


                                {REply ?
                                    <View style={styles.ReplyTemplete}>
                                        <View style={styles.ReplyTempleteHeader}>
                                            <View style={styles.ImageColumn12}>
                                                <Image source={require('../../../Images/manager2.png')}/>
                                            </View>
                                            <View style={styles.Titleandreply}>
                                                <Text style={styles.titleofotp1}>
                                                    Management Response
                                                </Text>
                                                <Text style={styles.UserComment}>
                                                    {this.props.detail.review_reply.text}
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                    :
                                    <View></View>
                                }


                                {
                                    ShowFooter ?
                                        <View style={styles.buttonsfooter}>

                                            {PartialRating ?
                                                <View></View>
                                                :
                                                <Button
                                                    onPress={this.popupbox.bind(this)}
                                                    transparent style={styles.buttonread}
                                                    textStyle={styles.buttontextstyle}>
                                                    <Icon style={styles.locationiconsize} name='md-folder-open'/>
                                                    <Text> Show Tags </Text>
                                                </Button>
                                            }

                                            {
                                                checkfunctionreturnvalue ?
                                                    <Button
                                                        onPress={this.reply.bind(this)}
                                                        transparent
                                                        style={styles.buttonread}
                                                        textStyle={styles.buttontextstyle}>
                                                        <Icon style={styles.locationiconsize} name='ios-chatbubbles'/>
                                                        <Text> Reply </Text>
                                                    </Button>
                                                    :
                                                    <View></View>
                                            }

                                            {
                                                ShowCreateIssuebutton ?
                                                    <Button
                                                        onPress={this.CreateIssuepopup.bind(this, Propertyname)}
                                                        transparent style={styles.buttonread}
                                                        textStyle={styles.buttontextstyle}>
                                                        <Icon style={styles.locationiconsize} name='md-open'/>
                                                        <Text> Create Issue </Text>
                                                    </Button>
                                                    :
                                                    <View></View>
                                            }
                                        </View>
                                        :
                                        <View style={styles.Nofooter}></View>
                                }


                                {
                                    this.state.Credentials ?
                                        <View style={styles.WarningView}>
                                            <Text style={styles.badgefont}>
                                                <Icon style={styles.locationiconsize} name='ios-warning'/>
                                                Please update OTA credentials
                                            </Text>
                                        </View>
                                        :
                                        <View></View>
                                }


                                {this.state.ShowReply ?
                                    <View style={styles.replyplusbuttonview}>

                                        <AutoGrowingTextInput
                                            style={styles.inputgroupstyle}
                                            placeholder={'Reply your Review Here'}
                                            onChangeText={(text) => this.setState({text})}
                                            value={this.state.text}
                                            underlineColorAndroid="transparent"
                                        />

                                        <View style={styles.REplybuttons}>

                                            <Button onPress={this.SubmitComment.bind(this)}
                                                    transparent
                                                    style={styles.buttonread}
                                                    textStyle={styles.buttontextstyle}>
                                                Post
                                            </Button>

                                            <Button onPress={this.SaveAsDraft.bind(this)}
                                                    transparent
                                                    style={styles.buttonread}
                                                    textStyle={styles.buttontextstyle}>
                                                Save Draft
                                            </Button>

                                            <Button onPress={this.showTemplates.bind(this)}
                                                    transparent
                                                    style={styles.buttonread}
                                                    textStyle={styles.buttontextstyle}>
                                                Templates
                                            </Button>

                                        </View>

                                    </View>
                                    :
                                    <View>
                                    </View>
                                }

                            </CardItem>
                        </Card>

                    </Content>
                </KeyboardAwareScrollView>
            )
        }
        else {
            // when review is twiiter
            return(
            <KeyboardAwareScrollView>

                <Content theme={myTheme}>
                    <Card style={styles.FeedbackRow}>

                        <CardItem>
                            <View style={styles.contentHeader}>
                                {this.Twitter_profile()}
                                <View style={styles.DetaisHeader}>
                                    <Text style={styles.titleofotp}>
                                        {this.props.detail.user.screen_name}
                                    </Text>
                                    <View style={styles.nameandrating}>
                                        <Text style={styles.username}>
                                            <Icon style={styles.locationiconsize} name='md-contacts'/>  {this.props.detail.user.name}
                                        </Text>
                                    </View>
                                    <Text style={styles.username}>
                                        <Icon style={styles.locationiconsize} name='ios-calendar-outline'/> {this.date_converter()}
                                        | {this.props.detail.source}
                                    </Text>
                                    <Text style={styles.username}>
                                         {this.props.detail.user.followers_count} Followers
                                        | {this.props.detail.user.statuses_count} Tweets
                                        | {this.props.detail.retweeted_count} Retweets
                                    </Text>
                                </View>
                            </View>


                            <View style={styles.Commentview}>
                                <Text style={styles.UserComment}>
                                    {this.props.detail.text}
                                </Text>
                            </View>

                        </CardItem>
                    </Card>

                </Content>
            </KeyboardAwareScrollView>

            )
        }

    }
}

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(ReviewsDetail);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.3)',
        paddingTop: 20,
    },

    Nofooter:{
      marginBottom:5,
    },

    WarningView:{
        flex:1,
        alignItems:"center",
        justifyContent:"center",
        marginTop:6,
        marginBottom:6
    },

    badgefont:{
       fontSize:11,
       backgroundColor:"rgba(255, 0, 0, 0.5)",
        paddingLeft:4,
        paddingRight:4,
        color:"#fff",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        paddingBottom:2,
        paddingTop:2,
    },

    FeedbackRow:{
        flex:1,
        marginLeft:7,
        marginRight:7,
        borderRadius: 5,
        padding: 4,
        shadowColor: '#000000',
        shadowOffset: {
            width: 5,
            height: 9
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        paddingBottom:0,
    },

    contentHeader:{
        flex:1,
        flexDirection:'row',
        marginBottom:10,
    },

    ImageColumn:{
        width: 45,
        height: 40,
        backgroundColor:'rgba(0,128,0,0.5)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginTop:6,
    },

    ImageTwitter:{
        width: 45,
        height: 40,
        // backgroundColor:'rgba(255, 0, 0,0.5)',
        // alignItems:"center",
        // justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginTop:6,
    },

    ImageColumnred:{
        width: 45,
        height: 40,
        backgroundColor:'rgba(255, 0, 0,0.5)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginTop:6,
    },

    ImageColumnblue:{
        width: 45,
        height: 40,
        backgroundColor:'rgba(102,204,204,0.99)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginTop:6
    },

    ImageColumn12:{
        width: 45,
        height: 40,
        borderRadius: 23,
        backgroundColor: '#fff',
        alignItems:"center",
        justifyContent:"center",
        // marginTop:6,
    },

    titleofotp:{
        fontSize: 13.5,
        marginLeft: 15,
        fontWeight:"bold",
        marginTop:3,
    },

    titleofotp1:{
        fontSize: 13.5,
        // marginLeft: 15,
        fontWeight:"bold",
    },

    DetaisHeader:{
        flex: 1,
        flexDirection: 'column',
    },

    username:{
        fontSize:12,
        marginLeft: 15,
        marginTop:2,
    },

    propertyname:{
        fontSize:12,
        marginLeft: 15,
    },

    RatingCards:{
        flex: 1,
        flexDirection: 'row',
    },

    OverallRating:{
        color:'#fff',
        fontWeight: 'bold',
        fontSize:12.5,
    },

    UserComment:{
        opacity:0.9,
        fontSize: 12.5,
    },

    buttonsfooter:{
        flex:1,
        flexDirection:'row',
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor: 'rgba(211,211,211,0.3)',
        height:22,
        paddingBottom:6,
        marginBottom:8,
    },


    buttonfooterblank:{
        flex:1,
        flexDirection:'row',
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
        // backgroundColor: 'rgba(211,211,211,0.3)',
        // height:22,
        // paddingBottom:6,
        marginBottom:6,
    },

    buttonread:{
        flex:0.3,
        // fontSize:7,
        alignSelf:"center",
        opacity:0.4,
        justifyContent: 'center',
    },

    Hotel:{
        fontSize:12,
        textAlign: "right",
        fontWeight:'bold',
        color:'blue',
        opacity:0.7,
    },

    texttags:{
        color:'blue',
    },

    buttontextstyle:{
        color:"rgb(0,128,0)",
        fontSize:11,
    },

    Replybuttoncolor:{
        fontSize:11,
        color:"#000",
    },

    locationiconsize:{
        fontSize:15,
        color:"#66CCCC",

    },

    propertyicon:{
        // color:"#66CCCC",
        backgroundColor:"#66CCCC",
    },

    replyplusbuttonview:{
        flex:1,
        flexDirection:"column",
        // alignItems: 'flex-start',
        justifyContent: 'space-around',
        // backgroundColor: 'rgba(211,211,211,0.3)',
    },

    Submitcommentbuuton:{
        // marginLeft:7,
        // // paddingBottom:15,
        backgroundColor:"#ffffff",
        borderColor:"#000",
        paddingTop:0,
        paddingBottom:0,

    },

    inputgroupstyle:{
        fontSize:12,
        borderBottomColor:'rgba(211,211,211,0.9)',
        borderBottomWidth: .5,
    },

    submiticonsize:{
        fontSize:19,
        color:"#66CCCC"
    },

    ReplyTemplete:{
        // marginLeft:10,
        marginTop:10,
    },

    ReplyTempleteHeader:{
        flexDirection:'row',
        // backgroundColor:"rgba(0, 0, 0, 0.2)",
    },

    Titleandreply:{
        flex:1,
        flexDirection:'column'
    },

    otpspace:{
        width: viewport.width * 0.10,
        alignItems: 'flex-end',
        height:30,
        marginTop:2,
    },

    REplybuttons:{
        flexDirection:'row',
        // justifyContent: 'space-around',
        backgroundColor: 'rgba(211,211,211,0.3)',
        height:22,
        flex:1,
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
        // backgroundColor: 'rgba(211,211,211,0.3)',
        // height:22,
        paddingBottom:6,
        marginBottom:8,
    }

});





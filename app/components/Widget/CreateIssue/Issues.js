import React, {Component} from 'react';
import { Container, Content, Card, CardItem, Icon, InputGroup, Input, List, ListItem,} from 'native-base';
import {StyleSheet,
    View,
    Navigator,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Modal,
    Picker,
    Item,
    Text} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';
import CreateIssue from './../../../containers/CreateIssue';

import Button from 'apsl-react-native-button';
const Dimensions = require('Dimensions');

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height - 25,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height-25,
};

class Issues extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            Count:'0',
            language:'',
        };
    }

    reply(){
        this.setState({
            ShowReply: true,
        });
    }

    createissue(){
        this.props.navigator.push({
            id:'CreateIssue'
        })
    }

    render() {

        let usercommentcheck;
        if(this.props.detail.UserComment != ''){
         usercommentcheck =
            <View style={styles.commentview}>
            <Text style={styles.UserComment}>
                {this.props.detail.UserComment}
            </Text>
            </View>
        }
        else {
            usercommentcheck =
                <View></View>
        }


        return (
            <Content theme={myTheme}>
                <Card style={styles.FeedbackRow} key={this.props.key}>

                    <CardItem>
                        <View style={styles.contentHeader}>
                            <View style={styles.ImageColumn}>
                                <Text style={styles.OverallRating}>
                                    {this.props.detail.OverallRating}
                                </Text>
                            </View>

                            <View style={styles.DetaisHeader}>

                                <Text style={styles.titleofotp}>
                                    {this.props.detail.Title}
                                </Text>

                                <View>
                                    <Text style={styles.username}>
                                        {this.props.detail.Username} |  {this.props.detail.UserCountry}
                                    </Text>
                                </View>

                                <Text style={styles.username}>
                                    {this.props.detail.Date}
                                </Text>

                            </View>

                            <View>
                                <Text>
                                {this.props.detail.OtpSource}
                                </Text>
                            </View>
                        </View>


                        {usercommentcheck}

                        <View style={styles.createissuesview}>

                          <InputGroup borderType='underline'>
                              <Input placeholder='Tags'/>
                          </InputGroup>


                            <Picker
                              selectedValue={this.state.language}
                              onValueChange={(lang) => this.setState({language: lang})} style={styles.pickerstyle}>
                                <Picker.Item label="Assigned To-"/>
                                <Picker.Item label="Front Desk" value="Front Desk" />
                                <Picker.Item label="Demo Account" value="Demo Account" />
                                <Picker.Item label="Komaldeep" value="Komaldeep" />
                            </Picker>


                            <InputGroup borderType='underline' style={styles.inputstyle}>
                                <Input placeholder='Keep In CC (Use it to send Email)'/>
                            </InputGroup>

                            <Button
                                style={{
                                    borderColor: '#02AFBD',
                                    borderRadius: 11,
                                    backgroundColor: '#02AFBD',
                                    justifyContent: 'center',
                                    width: viewport.width*0.8}}
                                    textStyle={{fontSize: 16, marginRight: 10, color: 'white'}} >
                                SUBMIT
                            </Button>
                        </View>
                    </CardItem>
                </Card>
            </Content>
        );
    }
}

const styles = StyleSheet.create({


    container: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.3)',
        paddingTop: 20,
    },

    FeedbackRow:{
        flex:1,
        marginLeft:7,
        marginRight:7,
    },

    contentHeader:{
        flex:1,
        flexDirection:'row',
        marginBottom:10,
    },

    ImageColumn:{
        width: 45,
        height: 40,
        backgroundColor:'rgba(0,128,0,0.5)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginTop:6,
    },


    titleofotp:{
        fontSize: 13,
        marginLeft: 15,
        fontWeight:"bold",
    },

    DetaisHeader:{
        flex: 1,
        flexDirection: 'column',
    },

    username:{
        fontSize:11,
        marginLeft: 15,
        marginTop:2,
    },

    RatingCards:{
        flex: 1,
        flexDirection: 'row',
    },

    OverallRating:{
        color:'#fff',
        fontWeight: 'bold',
        fontSize:11,
    },

    UserComment:{
        opacity:0.9,
        fontSize: 12,
    },

    buttonsfooter:{
        flex:1,
        flexDirection:'row',
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor: 'rgba(211,211,211,0.3)',
        height:22,
        paddingBottom:6,
        marginBottom:8,
    },

    buttonread:{
        flex:0.3,
        // fontSize:7,
        alignSelf:"center",
        opacity:0.4,
        justifyContent: 'center'
    },

    Hotel:{
        fontSize:12,
        textAlign: "right",
        fontWeight:'bold',
        color:'blue',
        opacity:0.7,
    },


    commentview:{
         paddingLeft:9,
    },


    buttonstyle:{
        marginTop:13,
    },

    alltags:{
        flexWrap:"wrap",
        alignItems:'center',
    },

    Discuss:{
        // flexDirection:'row',
        flexWrap:"wrap",
        alignItems:'center',
        paddingLeft:20,
        paddingRight:20,
    },

    insidetags:{
        flexDirection:'row',
        flexWrap:"wrap",
    },

    pickerstyle:{

    },

    createissuesview:{
        marginLeft:9,
        marginRight:15,
    },

    inputstyle:{
        marginBottom:20,
    }
});

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Issues);




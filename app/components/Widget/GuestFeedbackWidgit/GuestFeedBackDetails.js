import React, {Component} from 'react';
import { Container, Button, Item, Content, Card, CardItem, Icon, InputGroup, Input, ListItem, List } from 'native-base';
import {StyleSheet,
    View,
    Navigator,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Modal,
    Text} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';
import CreateIssue from './../../../containers/CreateIssue';
var moment = require('moment');
import {AfterInteractions} from 'react-native-interactions';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';

class GuestFeedBackDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            Count:'0',
            isreplible:true,
            text:'',
            AuthHeader:'',
            reply:false,
        };
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });
        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);
        this.setState({
            AuthHeader:Authobjects,
        });

    }

    reply(){
        this.setState({
            ShowReply: !this.state.ShowReply,
        });
    }

    CreateIssue(Propertyname){

        let Propertiesarray = this.props.Propertyarray;

            let filteredproperty = Propertiesarray.filter(
                (detail) => {
                    return detail.name == Propertyname
                }
            )


        let Property_id = filteredproperty[0]._id;

        this.props.Reviewid(this.props.detail._id , Property_id);
    }


    popupbox(){
        this.props.openpopupbox();
    }


    SubmitComment(){

        let Profile_id = this.props.profilematch;
        let ReplyText = this.state.text;
        let Review_id = this.props.detail._id;
        let AuthHeader = this.state.AuthHeader;

        this.setState({
            ShowReply:false,
            reply:true,
            isreplible:false,
        });

        this.props.actions.PushReviewReply(AuthHeader, Profile_id ,ReplyText ,Review_id);

    }

    SaveAsDraft(){

        let Profile_id = this.props.profilematch;
        let ReplyText = this.state.text;
        let Review_id = this.props.detail._id;
        let AuthHeader = this.state.AuthHeader;

        this.props.actions.PushReviewReplyDraft(AuthHeader, Profile_id ,ReplyText ,Review_id);

    }

    showTemplates(){


        let Propertiesarray = this.props.Propertyarray;
        let ReplyTemplates;


        if(Propertiesarray.length != 0) {

            let filteredproperty = Propertiesarray.filter(
                (detail) => {
                    return detail._id == this.props.detail.property_id
                }
            )

            // console.log(filteredproperty, 'filteredproperty');

            if (filteredproperty != undefined) {
                if(filteredproperty[0].reply_templates == undefined || filteredproperty[0].reply_templates == []) {
                    ReplyTemplates = [];
                }
                else {
                    ReplyTemplates = filteredproperty[0].reply_templates;
                }
            }
        }

        // console.log(ReplyTemplates,'ReplyTemplates');

        this.props.openTemplatebox(ReplyTemplates,this.props.ReviewFiedValue);


    }

    componentWillReceiveProps(props){

        if(props.ReviewsKey == this.props.ReviewFiedValue){
            if(props.TemplateText != ''){
                this.setState({
                    text: props.TemplateText
                });
            }
        }

        // console.log(props.detail.review_reply,'props.detail.review_reply.status');

        if(props.detail.review_reply != undefined) {

            if (props.detail.review_reply.status == "saved_as_draft" && props.TemplateText == '') {

                this.setState({
                    text: props.detail.review_reply.text
                });

            }
        }
    }

    CheckReplyButton_Show(){

        // let Allowed_Otp_Sources = ["booking","tripadvisor",
        //     "tripadvisor-attraction","holidayiq","goibibo","feedback","google"];
        //
        // let mySet = new Set(Allowed_Otp_Sources);
        // let OtpExist = mySet.has(this.props.detail.source); // true

        let Replybutton;
        if (this.state.isreplible == false ) {
            Replybutton = false;
        }
        else if (this.props.detail.review_reply == undefined) {
            Replybutton = true
        }
        else {
            Replybutton = false
        }

        if(this.props.detail.review_reply != undefined) {
            if (this.props.detail.review_reply.status == "saved_as_draft" && this.state.isreplible == true) {
                Replybutton = true;
            }
        }

        return Replybutton;
    }



    render() {

        if(this.state.ShowReply == true ){
            var ReplyBox =
                <View style={styles.replyplusbuttonview}>
                    <Input style={styles.inputgroupstyle}  placeholder='Reply your Review Here'/>

                    <TouchableOpacity style={styles.Submitcommentbuuton}>
                        <Icon style={styles.submiticonsize} name="md-arrow-dropright-circle"/>
                    </TouchableOpacity>
                </View>
        }
        else {
            var ReplyBox = <Text> </Text>
        }


        let dateString = this.props.detail.review_datetime;
        let date =   moment(dateString).format('DD/MM/YYYY');



        let Propertyname, OverRating, OverAllRating,filteredproperty;
        let Propertiesarray = this.props.Propertyarray;
        let showingcreateIssueButton = false;


        if(Propertiesarray.length != 0) {
            showingcreateIssueButton = true;
            filteredproperty = Propertiesarray.filter(
                (detail) => {
                    return detail._id == this.props.detail.property_id
                }
            )
            if (filteredproperty != undefined) {
                Propertyname = filteredproperty[0].name;
            }
        }


        // OverRating = false
        if(this.props.detail.review_data.ratings != undefined) {
            OverRating = true;
            OverAllRating = this.props.detail.review_data.ratings.filter(
                (detail) => {
                    return detail.name == "Overall Rating"
                }
            )

            if(OverAllRating[0] == undefined){
                OverRating = false;
            }
        }
        else {
            OverRating = false;
        }

        //console.log(this.props.detail.review_data.ratings,'this.props.detail.review_data.ratings')
        // if(OverRating == true){
        //     console.log(OverAllRating[0],'rating is')
        //     console.log(OverAllRating[0].rating,'OverAllRating is');
        // }
        // else {
        //     console.log("overtating is false");
        // }




        let REplyupward = false;
        let REply;

        if(this.state.reply == true){
            REplyupward = true
        }
        else if(this.props.detail.review_reply == undefined || this.props.detail.review_reply == ''){
            REply = false
        }
        else {
            if(this.props.detail.review_reply.status == "replied" || this.props.detail.review_reply.status == "processing" ) {
                REply = true
            }
            else {
                REply = false
            }
        }



        let checkfunctionreturnvalue = false;

        if(this.props.profiles.length != 0) {
            if(this.props.profiles[0].is_admin == true) {
                checkfunctionreturnvalue = this.CheckReplyButton_Show();
            }
            else {
                if(this.props.profiles[0].privileges.length != 0){
                    let Checkpost_reply = new Set(this.props.profiles[0].privileges);
                    let post_replyExist = Checkpost_reply.has("post_reply"); // true
                    if(post_replyExist == true){
                        checkfunctionreturnvalue = this.CheckReplyButton_Show();
                    }
                    else if(this.props.profiles[0].privileges.length == 0){
                        checkfunctionreturnvalue = this.CheckReplyButton_Show();
                    }
                }
            }
        }


        if(this.props.detail.review_reply != undefined) {
            checkfunctionreturnvalue = this.CheckReplyButton_Show();
        }



        return (

            <Content theme={myTheme}>
                <Card style={styles.FeedbackRow} key={this.props.key}>

                    <CardItem>
                        <View style={styles.contentHeader}>


                            {OverRating ?
                                <View style={styles.ImageColumn}>
                                    <Text style={styles.OverallRating}>
                                        {OverAllRating[0].rating}/5
                                    </Text>
                                </View>
                                :
                                <View></View>
                            }

                            <View style={styles.DetaisHeader}>

                                <Text style={styles.titleofotp}>
                                    {Propertyname}
                                </Text>

                                <View style={styles.nameandrating}>

                                    <Text style={styles.username}>
                                        <Icon style={styles.locationiconsize} name='md-contacts'/>   {this.props.detail.reviewer.name}  |  <Icon style={styles.locationiconsize} name='ios-mail-open'/>  {this.props.detail.reviewer.email}
                                    </Text>

                                </View>

                                <Text style={styles.username}>
                                    <Icon style={styles.locationiconsize} name='ios-calendar-outline'/> {date}
                                </Text>

                            </View>

                        </View>




                        <View style={styles.Listfooter}>

                            <List>
                                {
                                    this.props.detail.review_data.ratings.map((detail , i) => {
                                        return (
                                            <ListItem key={i} >
                                                <View style={styles.Listviewtable} key={i} >
                                                    <Text style={styles.listviewfontheader}> {detail.name}    </Text>
                                                    <Badge style={styles.albadges} key={i} textStyle = {styles.BadgeText}>
                                                        {detail.rating}
                                                    </Badge>
                                                </View>
                                            </ListItem>
                                        )
                                    })
                                }

                            </List>

                        </View>


                        <View>
                            <Text style={styles.UserComment}>
                                {this.props.detail.review_data.description}
                            </Text>
                        </View>


                        { REplyupward ?
                            <View style={styles.ReplyTemplete}>
                                <View style={styles.ReplyTempleteHeader}>
                                    <View style={styles.ImageColumn12}>
                                        <Image source={require('../../../Images/manager2.png')}/>
                                    </View>
                                    <View style={styles.Titleandreply}>
                                        <Text style={styles.titleofotp1}>
                                            Management Response
                                        </Text>
                                        <Text style={styles.UserComment}>
                                            {this.state.text}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            :
                            <View></View>
                        }


                        {REply ?
                            <View style={styles.ReplyTemplete}>
                                <View style={styles.ReplyTempleteHeader}>
                                    <View style={styles.ImageColumn12}>
                                        <Image source={require('../../../Images/manager2.png')}/>
                                    </View>
                                    <View style={styles.Titleandreply}>
                                        <Text style={styles.titleofotp1}>
                                            Management Response
                                        </Text>
                                        <Text style={styles.UserComment}>
                                            {this.props.detail.review_reply.text}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            :
                            <View></View>
                        }


                        {checkfunctionreturnvalue ?
                            <View style={styles.buttonsfooter}>
                                <Button
                                    onPress={this.reply.bind(this)}
                                    transparent
                                    style={styles.buttonread}
                                    textStyle={styles.buttontextstyle}>
                                    <Icon style={styles.locationiconsize} name='ios-chatbubbles'/>
                                    <Text> Reply </Text>
                                </Button>

                                { showingcreateIssueButton ?

                                <Button
                                    onPress={this.CreateIssue.bind(this , Propertyname)}
                                    transparent
                                    style={styles.buttonread}
                                    textStyle={styles.buttontextstyle}>
                                    <Icon style={styles.locationiconsize} name='ios-chatbubbles'/>
                                    <Text> Create-Issue </Text>
                                </Button>
                                :
                                <View> </View>
                                 }

                            </View>
                            :
                            <View>
                            </View>
                        }



                        {this.state.ShowReply ?
                            <View style={styles.replyplusbuttonview}>

                                <AutoGrowingTextInput
                                    style={styles.inputgroupstyle}
                                    placeholder={'Reply your Review Here'}
                                    onChangeText={(text) => this.setState({text})}
                                    value={this.state.text}
                                    underlineColorAndroid="transparent"
                                />

                                <View style={styles.REplybuttons}>

                                    <Button onPress={this.SubmitComment.bind(this)}
                                            transparent
                                            style={styles.buttonread}
                                            textStyle={styles.buttontextstyle}>
                                        Post
                                    </Button>

                                    <Button onPress={this.SaveAsDraft.bind(this)}
                                            transparent
                                            style={styles.buttonread}
                                            textStyle={styles.buttontextstyle}>
                                        Save Draft
                                    </Button>

                                    <Button onPress={this.showTemplates.bind(this)}
                                            transparent
                                            style={styles.buttonread}
                                            textStyle={styles.buttontextstyle}>
                                        Template
                                    </Button>

                                </View>

                            </View>
                            :
                            <View>
                            </View>
                        }

                    </CardItem>
                </Card>
            </Content>
            
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.3)',
        paddingTop: 20,
    },

    FeedbackRow:{
        flex:1,
        marginLeft:7,
        marginRight:7,

        borderRadius: 5,
        padding: 4,
        shadowColor: '#000000',
        shadowOffset: {
            width: 5,
            height: 9
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        paddingBottom:0,


    },

    contentHeader:{
        flex:1,
        flexDirection:'row',
        marginBottom:10,
    },

    ImageColumn:{
        width: 45,
        height: 40,
        backgroundColor:'rgba(0,128,0,0.5)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
        marginTop:6,
    },

    titleofotp:{
        fontSize: 13.5,
        marginLeft: 15,
        fontWeight:"bold",
        marginTop:3,
    },

    DetaisHeader:{
        flex: 1,
        flexDirection: 'column',
    },

    username:{
        fontSize:11,
        marginLeft: 15,
        marginTop:5,
    },

    RatingCards:{
        flex: 1,
        flexDirection: 'row',
    },

    OverallRating:{
        color:'#fff',
        fontWeight: 'bold',
        fontSize:13,
    },

    UserComment:{
        opacity:0.9,
        fontSize: 12,
    },

    buttonsfooter:{
        // flex:1,
        // flexDirection:'row',
        // marginTop:5,
        // marginTop:10,
        // backgroundColor: 'rgba(211,211,211,0.3)',
        // height:22,
        // paddingBottom:6,
        flex:1,
        flexDirection:'row',
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor: 'rgba(211,211,211,0.3)',
        height:22,
        paddingBottom:6,
        marginBottom:8,
    },

    buttonread:{
        flex:0.3,
        // fontSize:7,
        alignSelf:"center",
        opacity:0.4,
        justifyContent: 'center',
    },

    Hotel:{
        fontSize:12,
        textAlign: "right",
        fontWeight:'bold',
        color:'blue',
        opacity:0.7,
    },

    texttags:{
        color:'blue',
    },

    buttontextstyle:{
        color:"rgb(0,128,0)",
        fontSize:11,
    },

    locationiconsize:{
        fontSize:15,
        color:"#66CCCC"
    },

    Listfooter:{
        backgroundColor: 'rgba(211,211,211,0.11)',
        // paddingBottom:3,
        marginBottom:2,
    },

    listviewfontheader:{
         fontWeight:'bold',
        fontSize:11,
        opacity:0.9
    },

    listviewfont:{
         fontSize:12,
     },

    Listviewtable:{
       flexDirection:'row',
    },

    albadges:{
        backgroundColor:'rgba(102,204,204,0.99)',
        justifyContent: 'center'
    },

    BadgeText:{
        fontSize:10,
    },

    replyplusbuttonview:{
        flex:1,
        flexDirection:"column",
        // alignItems: 'flex-start',
        justifyContent: 'space-around',
    },

    Submitcommentbuuton:{
        justifyContent: 'flex-end',
        marginLeft:7,
        // paddingBottom:3,
    },

    inputgroupstyle:{
        fontSize:12,
        borderBottomColor:'rgba(211,211,211,0.9)',
        borderBottomWidth: .5,
        // minHeight:40,
        // paddingBottom:7,
        paddingTop:15,
    },

    submiticonsize:{
        fontSize:19,
        color:"#66CCCC"
    },

    REplybuttons:{
        flexDirection:'row',
        backgroundColor: 'rgba(211,211,211,0.3)',
        height:22,
        flex:1,
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
        paddingBottom:6,
        marginBottom:8,
    },
    ReplyTemplete:{
        // marginLeft:10,
        marginTop:10,
    },

    ReplyTempleteHeader:{
        flexDirection:'row',
        // backgroundColor:"rgba(0, 0, 0, 0.2)",
    },

    Titleandreply:{
        flex:1,
        flexDirection:'column'
    },

    ImageColumn12:{
        width: 45,
        height: 40,
        borderRadius: 23,
        backgroundColor: '#fff',
        alignItems:"center",
        justifyContent:"center",
        // marginTop:6,
    },

    titleofotp1:{
        fontSize: 13.5,
        // marginLeft: 15,
        fontWeight:"bold",
    },


});

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(GuestFeedBackDetails);




import React, {Component} from "react";
import { Container, Content, List, ListItem,CheckBox, InputGroup, Input, Button} from "native-base";
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, Image, ScrollView, Alert, Text, Picker, Item} from "react-native";
import {bindActionCreators} from "redux";
import * as counterActions from "../../../actions/DashboardActions";
import { connect } from "react-redux";
import GuestFeedBackDetails from "./GuestFeedBackDetails";
import DrawerLayoutAndroid from "DrawerLayoutAndroid";
import myTheme from "../../../Theme/Theme";
import PopupDialog, { DialogTitle } from 'react-native-popup-dialog';
import Badge from "react-native-smart-badge";
var Realm = require('realm');
import Dimensions from 'Dimensions';
import PropertyName from '../../Drawer/Feedback/Propertyname';
import Buttons from 'react-native-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CreateIssuePopup from '../../Drawer/Feedback/CreateIssuePopup';



class GuestFeedbackContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:"",
            optionSelected:"",
            ShowReply: false,
            clicked: false,
            selected1: "key1",
            language:"",
            All:false,
            Open:false,
            Closed:false,
            OtpProperties:false,
            MonthCal:'June2017',
            AuthHeader:'',
            properties:["all"],
            loadingDataPush:[],
            loading:6,
            Month:{"start":"2017-06-01", "end":"2017-06-30"},
            renderPlaceholderOnly: true,
            AllProperties:true,
            templatebody:[],
            ReviewsKey:"",
            Key:'',
            TemplateText:'',
            AlllSentiments:true,
            Dectractors:false,
            Promoters:false,
            Passive:false,
            nps:"all",
            AlllReply:true,
            Replied:false,
            NotReplies:false,
            replies:["all"],
            Reviewid:'',
            Issueid:'',
            Propertyid:'',
        };
    }

    handleScroll(event){
        var windowHeight = Dimensions.get('window').height,
            height = event.nativeEvent.contentSize.height,
            offset = event.nativeEvent.contentOffset.y;
        if( windowHeight + offset >= height ){
            let loadingnumber = this.state.loading;
            let addingnumber = 6;
            this.setState({
                loading: loadingnumber+ addingnumber,
            })
            this.LoadMoreData();
        }
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}}]
        });

        let backupdata = realm.objects('LoginAuth');
        let stringtoarray = backupdata[0].LoginAuthKey;
        let Authobjects = JSON.parse(stringtoarray);

        this.setState({
            AuthHeader:Authobjects,
        });

    }

    GetData(){
        let replies = this.state.replies;
        let category_mentions = ["all"];
        let date_range = this.state.Month;
        let nps = this.state.nps;
        let properties  = this.state.properties;
        let sources= ["feedback"];
        let tags = [];
        let limit = 50;
        let rating = -1;
        let skip = 0;

        this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
    }

    openDrawer() {
        this.refs["myDrawer"].openDrawer();
    }

    closeDrawer(){
        this.setState({
            clicked: true,
        })
    }

    openPopupBox(){
        this.popupDialog.openDialog();
    }

    DiscussPopupBox(){
        this.DiscussupDialog.openDialog();
    }

    LoadMoreData(){
        let replies = this.state.replies;
        let category_mentions = ["all"];
        let date_range = this.state.Month;
        let nps = this.state.nps;
        let properties  = this.state.properties;
        let sources= ["feedback"];
        let tags = [];
        let limit = this.state.loading;
        let rating = -1;
        let skip = 0;

        this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

    }

    All(){
        this.setState({
            All: !this.state.All,
        })
    }

    Open(){
        this.setState({
            Open: !this.state.Open,
        })
    }

    Closed(){
        this.setState({
            Closed : !this.state.Closed,
        })
    }

    propertynames(propertyid){

        this.state.properties.push(propertyid);
        let idx = this.state.properties.indexOf("all");

        if (idx != -1) {
            this.state.properties.splice(idx, 1);
        }
        this.setState({
            properties: this.state.properties,
            AllProperties:false
        });
        this.GetData();
        this.refs['myDrawer'].closeDrawer();

    }

    unchecked(event){

        let idx = this.state.properties.indexOf(event);

        if (idx != -1) {
            this.state.properties.splice(idx, 1);
        }
        this.setState({
            properties: this.state.properties
        })

        this.GetData();
        // this.ischeckemptyproperties();

    }

    PickerMontherData(lang){

        let MonthValue;

        if(lang == "June2017"){
            MonthValue = {
                "start":"2017-06-01",
                "end":"2017-06-30"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions = ["all"];
            let date_range = MonthValue;
            let nps = this.state.nps;
            let properties  = this.state.properties;
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "May2017"){
              MonthValue = {
                "start":"2017-05-01",
                "end":"2017-05-31"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions = ["all"];
            let date_range = MonthValue;
            let nps = this.state.nps;
            let properties  = this.state.properties;
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "April2017"){
            MonthValue = {
                "start":"2017-04-01",
                "end":"2017-04-30"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions = ["all"];
            let date_range = MonthValue;
            let nps = this.state.nps;
            let properties  = this.state.properties;
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang =="March2017"){

            MonthValue = {
                "start":"2017-03-01",
                "end":"2017-03-31"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;;
            let category_mentions = ["all"];
            let date_range = MonthValue;
            let nps = this.state.nps;
            let properties  = this.state.properties;
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

            this.refs['myDrawer'].closeDrawer();

        }
        else if(lang == "Februry2017"){

            MonthValue = {
                "start":"2017-02-01",
                "end":"2017-02-28"
            };

            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions = ["all"];
            let date_range = MonthValue;
            let nps = this.state.nps;
            let properties  = this.state.properties;
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);


            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "January2017"){

            MonthValue={
                "start":"2017-01-01",
                "end":"2017-01-31"
            };
            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions = ["all"];
            let date_range = MonthValue;
            let nps = this.state.nps;
            let properties  = this.state.properties;
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);


            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "December2016"){

            MonthValue={
                "start":"2016-12-01",
                "end":"2016-12-31"
            };
            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions = ["all"];
            let date_range = MonthValue;
            let nps = this.state.nps;
            let properties  = this.state.properties;
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();
        }
        else if(lang == "November2016"){

            MonthValue={
                "start":"2016-11-01",
                "end":"2016-11-30"
            };
            this.setState({
                Month: MonthValue,
                MonthCal:lang
            });

            let replies = this.state.replies;
            let category_mentions = ["all"];
            let date_range = MonthValue;
            let nps = this.state.nps;
            let properties  = this.state.properties;
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
            this.refs['myDrawer'].closeDrawer();

        }
    }

    PressAlProperties(){

        if(this.state.AllProperties == false) {
            this.setState({
                properties:["all"],
                AllProperties:true,
            });


            let replies = this.state.replies;
            let category_mentions = ["all"];
            let date_range = this.state.Month;
            let nps = this.state.nps;
            let properties  = ["all"];
            let sources= ["feedback"];
            let tags = [];
            let limit = 50;
            let rating = -1;
            let skip = 0;

            this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);
        }

        else {
            this.setState({
                AllProperties:false
            });
        }
        this.refs['myDrawer'].closeDrawer();

    }


    Templetesbox(templatebody,ReviewsKey){

        this.setState({
            templatebody,
            Key:ReviewsKey,
        });

        this.TemplatepopupDialog.openDialog();

    }

    SelectedTemplates(TemplateName){

        console.log(TemplateName,'TemplateName is');
        this.setState({
            TemplateText:TemplateName,
            ReviewsKey:this.state.Key,
        });
        this.TemplatepopupDialog.closeDialog();
    }


    LoadingdataonRequest(npsdata, replieddata){

        // console.log(npsdata , 'npsdata');
        // console.log(replieddata , 'replieddata');

        let replies = replieddata;
        let category_mentions= ["all"];
        let date_range = this.state.Month;
        let nps= npsdata;
        let properties = this.state.properties;
        let sources = ["feedback"];
        let tags = [];
        let limit = 6;
        let rating = -1;
        let skip = 0;

        this.props.actions.GuestfeedbackList(this.state.AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip);

    }

    AlllSentiments(){
        this.setState({
            AlllSentiments:true,
            Dectractors:false,
            Passive:false,
            Promoters:false,
            nps:"all",
        });
        let nps = "all";
        let replieddata = this.state.replies;
        this.LoadingdataonRequest(nps, replieddata);
        // this.GetData();
        this.refs['myDrawer'].closeDrawer();
    }

    Dectractors(){
        this.setState({
            Dectractors:true,
            AlllSentiments:false,
            Passive:false,
            Promoters:false,
            nps:-1,
        });
        let nps = -1;
        let replieddata = this.state.replies;
        this.LoadingdataonRequest(nps, replieddata);

        // this.GetData();
        this.refs['myDrawer'].closeDrawer();
    }

    Passive(){
        this.setState({
            Dectractors:false,
            AlllSentiments:false,
            Passive:true,
            Promoters:false,
            nps: 0,
        });
        let nps = 0;
        let replieddata = this.state.replies;
        this.LoadingdataonRequest(nps, replieddata);

        // this.GetData();
        this.refs['myDrawer'].closeDrawer();

    }

    Promoters(){
        this.setState({
            Dectractors:false,
            AlllSentiments:false,
            Passive:false,
            Promoters:true,
            nps: 1,
        });
        let nps = 1;
        let replieddata = this.state.replies;
        this.LoadingdataonRequest(nps, replieddata);
        // this.GetData();
        this.refs['myDrawer'].closeDrawer();
    }

    AlllReply(){

        this.setState({
            AlllReply:true,
            Replied:false,
            NotReplies:false,
            replies:["all"],
        });

        let npsdata = this.state.nps;
        let replieddata = ["all"];
        this.LoadingdataonRequest(npsdata, replieddata);
        this.refs['myDrawer'].closeDrawer();

    }

    Replied(){

        this.setState({
            AlllReply:false,
            Replied:true,
            NotReplies:false,
            replies:"replied",
        });

        let npsdata = this.state.nps;
        let replieddata = "replied";
        this.LoadingdataonRequest(npsdata, replieddata);
        this.refs['myDrawer'].closeDrawer();

    }

    NotReplies(){

        this.setState({
            AlllReply:false,
            Replied:false,
            NotReplies:true,
            replies:"notreplied",
        });

        let npsdata = this.state.nps;
        let replieddata = "notreplied";
        this.LoadingdataonRequest(npsdata, replieddata);
        this.refs['myDrawer'].closeDrawer();

    }

    Reviewid(Issueid, reviewid){

        this.setState({
            Issueid,
            Propertyid:reviewid
        });

        this.Issuepopuxbox.openDialog();

    }

    Closeissuepopupbox(close){

        this.Issuepopuxbox.closeDialog();

    }



    render() {
        const { GuestList , propertieslist, profile } = this.props.Guestfeeeback;

        let showdata , realobjectlength , properties, Propertyarray, profilematch, alltemplates, Sentiments, Reply_status;

        let profiles = [];
        if(profile != false) {

            for (let i in profile.profile) {
                profiles.push(profile.profile[i]);
            }
            profilematch = profiles[0]._id;

        }

        //popupbox with templates
        if(this.state.templatebody.length == 0 ){
            alltemplates =  <List>
                <ListItem>
                    <Text>
                        Sorry, No Template is available for this Property
                    </Text>
                </ListItem>
            </List>
        }
        else {

            alltemplates = <List>
                {
                    this.state.templatebody.map(
                        (detail , i) => {
                            return(
                                <ListItem key={i}>

                                    <Buttons onPress={this.SelectedTemplates.bind( this, detail.body)} key={i}>
                                        <Text>
                                            {detail.title}
                                        </Text>
                                    </Buttons>

                                </ListItem>
                            )
                        }
                    )
                }
            </List>
        }

        Propertyarray = [];
        if(propertieslist ==  false){
            properties =
                <ListItem>
                    <Text> Loading ... </Text>
                </ListItem>
        }
        else {

            for (let i in propertieslist.Properties.data) {
                Propertyarray.push(propertieslist.Properties.data[i]);
            }


            properties =
                <List>
                    <ListItem>

                        {
                            this.state.AllProperties ?
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.PressAlProperties.bind(this)}>
                                    <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={styles.FilterTouch} onPress={this.PressAlProperties.bind(this)}>
                                    <Image style={styles.Imagestyle} source={require('./../../../Images/unchecked.png')}/>
                                    <Text style={styles.CheckboxText}> All </Text>
                                </TouchableOpacity>
                        }

                    </ListItem>

                    {
                        Propertyarray.map((detail, i) => {
                        return (
                            <PropertyName
                                key={i}
                                item={detail}
                                propertyname={this.propertynames.bind(this)}
                                uncheckedpropertyname={this.unchecked.bind(this)}
                            />
                          )
                        })
                    }
                </List>


            Sentiments =
                <List>
                    <ListItem itemDivider>
                        <Text> Sentiments </Text>
                    </ListItem>

                    <ListItem>
                        {this.state.AlllSentiments ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.AlllSentiments.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.AlllSentiments.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.Dectractors ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Dectractors.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Detractors </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Dectractors.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Detractors </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.Passive ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Passive.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Passive </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Passive.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Passive </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.Promoters ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Promoters.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Promoters </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Promoters.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Promoters </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>
                </List>




            Reply_status =
                <List>
                    <ListItem itemDivider>
                        <Text> Reply Status </Text>
                    </ListItem>

                    <ListItem>
                        {this.state.AlllReply ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.AlllReply.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.AlllReply.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> All </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.Replied ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Replied.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Replied </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.Replied.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Replied </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                    <ListItem>
                        {this.state.NotReplies ?
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.NotReplies.bind(this)}>
                                <Image style={styles.fadebox} source={require('./../../../Images/check1.png')}/>
                                <Text style={styles.CheckboxText}> Not Replied </Text>
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.FilterTouch} onPress={this.NotReplies.bind(this)}>
                                <Image style={styles.Imagestyle}
                                       source={require('./../../../Images/unchecked.png')}/>
                                <Text style={styles.CheckboxText}> Not Replied </Text>
                            </TouchableOpacity>
                        }
                    </ListItem>

                </List>
        }



        if(GuestList == false){

            realobjectlength = <View style={styles.ViewLoading}>
                <Text style={styles.loading}> Loading... </Text>
            </View>

        }

        else {

            realobjectlength = <View></View>;

            let ReviewsFeedarray = [];
            for (let i in GuestList.guestfeedbackdata.data) {
                ReviewsFeedarray.push(GuestList.guestfeedbackdata.data[i]);
            }

            if (ReviewsFeedarray.length == 0) {
                showdata = <View style={styles.ViewLoading}>
                    <Text> Sorry, No Review is available  </Text>
                </View>
            }

            else {

                showdata =
                    ReviewsFeedarray.map((detail, i) => {

                        return (
                            <GuestFeedBackDetails
                                openpopupbox={this.openPopupBox.bind(this)}
                                navigator={this.props.navigator}
                                Propertyarray={Propertyarray}
                                detail={detail}
                                key={detail._id}
                                ReviewFiedValue={detail._id}
                                openTemplatebox={this.Templetesbox.bind(this)}
                                TemplateText={this.state.TemplateText}
                                ReviewsKey={this.state.ReviewsKey}
                                profilematch={profilematch}
                                profiles={profiles}
                                Reviewid={this.Reviewid.bind(this)}
                            />
                        )
                    })
            }
        }

        var navigationView = (
            <Container style={styles.filterbackground}>
                <Content theme={myTheme} >

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Filters </Text>
                    </View>


                    <List>

                        <ListItem>
                            <Picker
                                selectedValue={this.state.MonthCal}
                                onValueChange={this.PickerMontherData.bind(this)}>

                                <Picker.Item label="June 2017" value="June2017" />
                                <Picker.Item label="May 2017" value="May2017" />
                                <Picker.Item label="April 2017" value="April2017" />
                                <Picker.Item label="March 2017" value="March2017" />
                                <Picker.Item label="February 2017" value="Februry2017" />
                                <Picker.Item label="January 2017" value="January2017" />
                                <Picker.Item label="December 2016" value="December2016" />
                                <Picker.Item label="November 2016" value="November2016" />

                            </Picker>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text> Properties </Text>
                        </ListItem>

                        {properties}
                        {Sentiments}
                        {Reply_status}


                    </List>

                </Content>
            </Container>
        );

        return (

            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={240}
                drawerPosition={DrawerLayoutAndroid.positions.Right}
                renderNavigationView={() => navigationView} >

                <ScrollView
                    showsVerticalScrollIndicator ={true}
                    onScroll={this.handleScroll.bind(this)}
                    style={styles.overcontainer}>

                    <View style={styles.Filterbar}>

                    <Buttons onPress={this.openDrawer.bind(this)}>
                        <Image source={require('../../../Images/filter2.png')}/>
                        <Text style={styles.Filter}>  Filter </Text>
                    </Buttons>
                    </View>

                    {showdata}

                </ScrollView>


                <PopupDialog
                    ref={(TemplatepopupDialog) => { this.TemplatepopupDialog = TemplatepopupDialog; }}
                    width= {320}
                    height={250}>

                    <ScrollView style={styles.alltags}>
                        {alltemplates}
                    </ScrollView>

                </PopupDialog>



                <PopupDialog
                    dialogTitle={<DialogTitle title="Create Issue" />}
                    ref={(Issuepopuxbox) => { this.Issuepopuxbox = Issuepopuxbox; }}
                    width= {320}
                    height={480}>

                    <CreateIssuePopup
                        Issueid={this.state.Issueid}
                        Propertyid={this.state.Propertyid}
                        Propertyarray={Propertyarray}
                        Source="feedback"
                        Closepopupbox={this.Closeissuepopupbox.bind(this)}
                    />

                </PopupDialog>



            </DrawerLayoutAndroid>

        );
    }
}

const styles = StyleSheet.create({

    overcontainer: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.2)',
    },

    Filterbar:{
        marginRight: 10,
        marginBottom:2,
        alignItems:"flex-end",
    },

    Filter:{
            fontSize:16,
            color:"black",
            alignItems:'flex-end',
    },

    arrowstyle:{
        marginTop:14,
        marginLeft:8,
    },

    FilterHeader:{
        backgroundColor:'#02afbd',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    albadges:{
        backgroundColor:"rgba(102,178,255,1)",
    },

    Discuss:{
        flexWrap:"wrap",
        paddingLeft:20,
        paddingRight:20,
    },

    inputtags:{
        marginBottom:20,
        marginTop:20,
    },

    filterbackground:{
        backgroundColor:"rgba(2,59,79,0.0)",
    },

    FilterTextColor:{
        color:"#fff"
    },

    loading:{
        alignItems:"center",
        color:"#fff",
        fontSize:15,
    },

    ViewLoading:{
        flex:1,
        alignItems:"center",
        // backgroundColor:"rgba(0, 0, 0, 0.3)",
        height:35,
        justifyContent: 'center',
    },

    FilterTouch:{
        flexDirection:"row",
        marginTop:4,
        marginLeft:4,
    },

    CheckboxText:{
        marginLeft:6,
    }

})


export default connect(Guestfeeeback => ({
        Guestfeeeback: Guestfeeeback.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(GuestFeedbackContainer);
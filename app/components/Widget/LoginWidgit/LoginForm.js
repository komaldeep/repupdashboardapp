import React, {Component} from 'react';
import { Container, Content, Form, Item, Input, Label } from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight, Image, ScrollView, Alert, Text,Picker,Spinner,ListView,TextInput,KeyboardAvoidingView, PermissionsAndroid} from 'react-native';
import Button from 'apsl-react-native-button';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
const Dimensions = require('Dimensions');
var Realm = require('realm');
import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';
import { Actions } from 'react-native-router-flux';
import Pushy from 'pushy-react-native';

let TajId= [
    'cloud+taj@repup.co',
    'geet.nazir@tajhotels.com',
    'ritika.gupta@tajhotels.com',
    'devendra.singhmehta@tajhotels.com',
    'vishal.sekhri@tajhotels.com',
    'satish.rajput@tajhotels.com',
    'raja.sadhukhan@tajhotels.com',
    'dev.gautam@tajhotels.com',
    'cloud+swarna@repup.co',
    'saurabh.singh@tajhotels.com',
    'vishwajyoti.goswami@tajhotels.com',
    'amit.tandon@tajhotels.com',
    'chatterjee.pritha@tajhotels.com',
    'rk.gurung@tajhotels.com',
    'akashdeep.mathur@tajhotels.com',
    'mukesh.chaudhary@tajhotels.com',
    'rikhe.claudius@tajhotels.com',
    'sumeet.taneja@tajhotels.com',
    'sandeep.shekhawat@tajhotels.com'
]

const viewport = {
    fullWidth: Dimensions.get('window').width,
    fullHeight: Dimensions.get('window').height - 25,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height-25,
};

Pushy.setNotificationListener(async (data) => {

    console.log('Received notification: ' + JSON.stringify(data));

});



class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            hasErrors: false,
            attemptingLogin: false,
            error: '',
            username: '',
            password: '',
            Error:'',
            ServerResponse:'',
            LoadingBars:false,
            tokenvalue:'',
            FirebaseToken:'',
            DeviceToken:"",
        };
    }

    componentWillMount(){

        let realm = new Realm({
            schema: [{name: 'LoginAuth', properties: {LoginAuthKey: 'string'}},
                {name: 'Usernname', properties: {AuthUsernname: 'string'}}
            ]
        });

        let backupdata = realm.objects('LoginAuth');
        let AuthUsername = realm.objects('Usernname');

       // console.log(backupdata, 'backupdata');
       // console.log(AuthUsername, 'AuthUsername');

        if(backupdata[0] != undefined) {

            let stringtoarray = backupdata[0].LoginAuthKey;
            let Authobjects = JSON.parse(stringtoarray);

            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth() + 1; //January is 0!
            let yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }
            today = yyyy + '-' + mm + '-' + dd;

            if(Authobjects.expires < today){

            }
            else{
                let Username = AuthUsername[0].AuthUsernname;

                console.log(Username,'USERNAME');

                if(TajId.includes(Username) == true){
                    console.log('it comes here')
                    this.props.actions.taj_product();
                }
                 Actions.ReviewsFeed();
            }

        }
    }

    componentDidMount(){
         Pushy.listen();

         // Check whether the user has granted the app the WRITE_EXTERNAL_STORAGE permission
          PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((granted) => {
            if (!granted) {
          // Request the WRITE_EXTERNAL_STORAGE permission so that the Pushy SDK will be able to persist the device token in the external storage
           PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
          // User denied permission?
            if (result !== PermissionsAndroid.RESULTS.GRANTED) {
            // Possibly ask the user to grant the permission
            }
         });
         }
        });



          Pushy.register().then(async (deviceToken) => {
          // Print device token to console
           console.log('Pushy device token: ' + deviceToken);

           this.setState({
            DeviceToken:deviceToken
           });

          }).catch((err) => {
          console.error(err);
          });

    }

    SendRegistration_id(AuthKeyPath , token){

        console.log('it comes in Send Resgistration_id');

        fetch('https://cloud.repup.co/api/register_gcm_android', {
            method: 'POST',
            headers: {
                'auth-key': AuthKeyPath.auth_key,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "registration_id": token,
                "platform":"android"
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson,'response json');
            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });

    }

    CampareRegistrationid(AuthKeyPath){

        let token  = this.state.DeviceToken;

        let realm = new Realm({
            schema: [{name: 'FirebaseAuthToken', properties: {key: 'string'}}]
        });

        if(token == null || token == undefined){
            console.log('token is not defined 1st');
        }
        else {

            let objectlength = realm.objects('FirebaseAuthToken').length;
            if (objectlength >= 1) {

                let firebasetoken = realm.objects('FirebaseAuthToken');

                if(firebasetoken[0].key == token){
                    console.log('value matches');
                    // no need to sent to server, it contain token from last insertion
                }
                else {

                    // if values doesnt match we need to sent asychrouns request
                    this.SendRegistration_id(AuthKeyPath , token);
                    console.log('value didnt match');

                }
            }
            else {

                console.log('First time login');
                realm.write(() => {
                    realm.create('FirebaseAuthToken', {key: token});
                });

                // sent post request with value
                this.SendRegistration_id(AuthKeyPath , token);

            }

        }

    }

    SavingAuthValue(AuthKeyPath, Usernname){

        let realm = new Realm({
            schema: [
                {name: 'LoginAuth', properties: {LoginAuthKey: 'string'}},
                {name: 'Usernname', properties: {AuthUsernname: 'string'}}
                ]
        });

        let stringyfyobject = JSON.stringify(AuthKeyPath);
        let stringy_username = JSON.stringify(Usernname);

        realm.write(() => {
            realm.create('LoginAuth', {LoginAuthKey: stringyfyobject});
            realm.create('Usernname', {AuthUsernname: Usernname});
        });

        let objectlength = realm.objects('LoginAuth').length;

        if (objectlength > 1) {
            realm.write(() => {
                let backupdata = realm.create('LoginAuth', {LoginAuthKey: stringyfyobject});
                let backupdata1 = realm.create('Usernname', {AuthUsernname: Usernname});
                let allobjects = realm.objects('LoginAuth');
                let allobjects1 = realm.objects('Usernname');
                realm.delete(allobjects);
                realm.delete(allobjects1);
                realm.create('LoginAuth', {LoginAuthKey: stringyfyobject});
                realm.create('Usernname', {AuthUsernname: Usernname});
            });
        }

        this.CampareRegistrationid(AuthKeyPath);

    }

    async attemptLogin() {

        // console.log('it comes inside attempt login');

        let Usernname = this.state.username;
        let password = this.state.password;


        this.setState({
            LoadingBars:true,
        });

            await fetch('https://cloud.repup.co/api/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "email": Usernname,
                    "password": password,
                })
            }).then((response) => response.json())
                .then((responseJson) => {

                    if(responseJson.success == true){
                        this.setState({
                            LoadingBars:false,
                            ServerResponse: responseJson
                        });

                        if(TajId.includes(Usernname) == true){
                            this.props.actions.taj_product();
                        }

                        this.SavingAuthValue(responseJson, Usernname);
                        Actions.ReviewsFeed();
                    }
                    else {
                        this.setState({
                            Error:"Username Or Password is Wrong",
                            LoadingBars:false,
                        })
                    }

                })
                .catch((error) => {
                    this.setState({
                        LoadingBars:false,
                    })
                    return false;
                });

    }

    render() {

        let Errors, loginform;
        if(this.state.Error == ''){
            Errors= <Text> </Text>
        }
        else {
            Errors =  <Text style={styles.ErrorText}>**{this.state.Error}**</Text>
        }


        return (
            <View>
                <View
                    style={styles.loginContainer}>

                    <View style={styles.ErrorView}>
                        {Errors}
                    </View>

                    <TextInput
                        underlineColorAndroid="transparent"
                        style={styles.loginTextInputs1}
                        onChangeText={(text) => this.setState({username: text})}
                        ref={'username'}
                        placeholder="Enter your Username"/>


                    <TextInput
                        underlineColorAndroid="transparent"
                        secureTextEntry={true}
                        style={styles.loginTextInputs1}
                        onChangeText={(text) => this.setState({password: text})}
                        ref={'password'}
                        placeholder="Enter your password"/>

                </View>
                <View>
                    <Button
                        style={{
                            borderColor: '#02AFBD',
                            borderRadius: 11,
                            backgroundColor: '#02AFBD',
                            justifyContent: 'center',
                            marginTop: viewport.height * 0.05,
                            width: viewport.width * 0.8
                        }}
                        textStyle={{fontSize: 16, marginRight: 10, color: 'white'}}
                        onPress={this.attemptLogin.bind(this)}>
                        LOGIN
                    </Button>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    loginView: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: viewport.height
    },

    loginContainer: {
        marginTop: viewport.height*0.1,
        width: viewport.width*0.8
    },

    loginTextInputs1: {
        marginTop: 20,
        borderBottomColor: 'black',
        borderBottomWidth: .5,
    },

    buttonLogin: {
        backgroundColor: 'white',
        marginTop: viewport.height*0.1,
        width: viewport.width*0.5
    },

    loginResponseView: {
        flexDirection: 'row',
        marginTop: -5
    },

    ErrorText:{
        color:"red",
    },

    ErrorView:{
        // alignItems:"center",
    }

})



export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(LoginForm);


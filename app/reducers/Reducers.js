

const initialState = {
    count: 0,
    Restdata: false,
    ReviewsFeed: false,
    Issueslist:false,
    GuestList:false,
    propertieslist: false,
    teamlist:false,
    agodalist:false,
    bookinglist:false,
    goibibolist:false,
    propertyfeedback:false,
    SuccessStory:false,
    ClosedIssue:false,
    Assignedto:false,
    Loginpanel:false,
    loader:'',
    profile:false,
    taj:false,
};


export default function counter(state = initialState, action = {}) {
  switch (action.type) {

      case "TAJPRODUCT":
          return {
              ...state,
              taj:action.payload
          }

      case "RESTDATA":
          return {
              ...state,
              Restdata: action.payload
          };

      case "REVIEWSFEED":
          return {
              ...state,
              ReviewsFeed: action.payload
          }

      case "ERROR":
          return {
              ...state,
              Error: action.payload
          }

      case "ISSUESLIST":
          return {
              ...state,
              Issueslist:action.payload
          }

      case "GUESTFEEDBACKLIST":
          return {
              ...state,
              GuestList:action.payload
          }

      case "PROPERTIES":
          return {
              ...state,
              propertieslist:action.payload
          }
      case "TEAM":
          return {
              ...state,
              teamlist:action.payload
          }

      case "AGODA":
          return {
              ...state,
              agodalist:action.payload
          }

      case "BOOKING":
          return {
              ...state,
              bookinglist:action.payload
          }

      case "GOIBIBO":
          return {
              ...state,
              goibibolist:action.payload
          }

      case "OTPROPERTYFEEDBACK":
          return {
              ...state,
              propertyfeedback: action.payload
          }

      case "POSTFEEDBACKREPLY":
          return {
              ...state,
              SuccessStory:action.payload
          }

      case "CLOSEDISSUE":
          return {
              ...state,
              ClosedIssue :action.payload
          }

      case "ASSIGNEDTO":
          return {
              ...state,
              Assignedto:action.payload
          }

      case "SURITYTOCLOSEISSUE":
          return {
              ...state,
              SureClosed:action.payload
          }

      case "SUCESSLOGIN":
          return {
              ...state,
              Loginpanel:action.payload
          }

      case 'LOADER': {
          return {
              ...state,
              loader: action.payload
          }
      }

      case 'PROFILE': {
          return {
              ...state,
              profile:action.payload
          }
      }

    default:
      return state;

  }
}

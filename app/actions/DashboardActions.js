// import { mapInFrames } from 'next-frame';
import nextFrame from 'next-frame';
var loadingrequestfetchdatacheckingdata;



export function taj_product() {

    return  {
        type: "TAJPRODUCT",
        payload: {
            tajactive: true
        }
    }

}

export function CreateIssue(AuthHeader,ccids,assigned_to,review_id, source, tags) {

    return async function (dispatch) {

        await nextFrame();

        await fetch('https://cloud.repup.co/api/create_issue', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':AuthHeader.auth_key
            },
            body: JSON.stringify({
            "ccids":ccids,
            "assigned_to":assigned_to,
            "review_id":review_id,
            "source":source,
            "tags":tags,
                 "guest_info":{
                    "name": "",
                    "room_number": "",
                    "checkin": "",
                    "checkout": ""
                 }
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson , 'responseJson');
            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }

}


export function closeIssue(AuthHeader, Issueid, ClosingText) {

    return async function (dispatch) {
        await nextFrame();

        await fetch('https://cloud.repup.co/api/update_issue_status', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':AuthHeader.auth_key
            },
            body: JSON.stringify({
                new_status: "closed",
                issue_id: Issueid,
                closure_note: ClosingText
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson , 'responseJson1');
            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }

}


export function openIssue(AuthHeader, Issueid) {

    return async function (dispatch) {
        await nextFrame();

        await fetch('https://cloud.repup.co/api/update_issue_status', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':AuthHeader.auth_key
            },
            body: JSON.stringify({
                new_status: "open",
                issue_id: Issueid,
            })
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson , 'responseJson1');
            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }

}

export function ReviewsFeeddata(AuthHeader,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip) {

    return async function (dispatch) {

        await nextFrame();
        // loadingrequest = true;

        dispatch ({
            type: "LOADER",
            payload: {
                loadingaction: false,
            }
        });

        await fetch('https://cloud.repup.co/api/reviews', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':AuthHeader.auth_key
            },
            body: JSON.stringify({
                "replies":replies,
                "category_mentions":category_mentions,
                "date_range":date_range,
                "date_type": "review",
                "nps": nps,
                "properties":properties,
                "sources":sources,
                "tags":tags,
                "limit":limit,
                "rating":rating,
                "skip":skip
            })
        }).then((response) => response.json())
            .then((responseJson) => {

                dispatch ({
                    type: "REVIEWSFEED",
                    payload: {
                        reviewsfeeddata: responseJson
                    }
                })

                dispatch ({
                    type: "LOADER",
                    payload: {
                        loadingaction: true,
                    }
                })

            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }

}

export function Profile(Authobjects){

    return async function (dispatch) {

        await nextFrame();

        await fetch('https://cloud.repup.co/api/profile', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':Authobjects.auth_key
            }
        }).then((response) => response.json())
            .then((responseJson) => {

                dispatch ({
                    type: "PROFILE",
                    payload: {
                        profile:responseJson
                    }
                })

            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }

}

export function PushReviewReply(AuthHeader, Profile_id ,ReplyText ,Review_id) {

    return async function (dispatch) {

        await nextFrame();

        await fetch('https://cloud.repup.co/api/post_reply', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':AuthHeader.auth_key
            },
            body: JSON.stringify({
                "review_id":Review_id,
                "replyText":ReplyText,
                "user_id":Profile_id
            })
        }).then((response) => response.json())
            .then((responseJson) => {


            console.log(responseJson,'responseJson in post')

                // dispatch ({
                //     type: "PROFILE",
                //     payload: {
                //         profile:responseJson
                //     }
                // })

            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }
}


export function PushReviewReplyDraft(AuthHeader, Profile_id ,ReplyText ,Review_id) {

    return async function (dispatch) {

        await nextFrame();

        await fetch('https://cloud.repup.co/api/post_reply', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':AuthHeader.auth_key
            },
            body: JSON.stringify({
                "review_id":Review_id,
                "replyText":ReplyText,
                "user_id":Profile_id,
                "is_draft":true
            })
        }).then((response) => response.json())
            .then((responseJson) => {


                console.log(responseJson,'responseJson in post')

                // dispatch ({
                //     type: "PROFILE",
                //     payload: {
                //         profile:responseJson
                //     }
                // })

            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }
}



export function IssuesList(Authobjects , properties , status, assigned_to, sources, skip, limit) {


    return async function (dispatch) {

        await nextFrame();

        dispatch ({
            type: "LOADER",
            payload: {
                loadingaction: false,
            }
        })

        await fetch('https://cloud.repup.co/api/issues', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':Authobjects.auth_key
            },
            body: JSON.stringify({
            "properties":properties,
            "status":status,
            "assigned_to":assigned_to,
            "sources":sources,
            "skip":skip,
            "limit": limit,
            })
        }).then((response) => response.json())
            .then((responseJson) => {

                dispatch ({
                    type: "ISSUESLIST",
                    payload: {
                        Issuesreviewsfeeddata:responseJson
                    }
                })

                dispatch ({
                    type: "LOADER",
                    payload: {
                        loadingaction:true
                    }
                })

            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }

}


export function GuestfeedbackList(Authobjects,replies,category_mentions,date_range,nps,properties,sources,tags,limit,rating,skip) {



    return async function (dispatch) {
        await nextFrame();

        dispatch ({
            type: "LOADER",
            payload: {
                loadingaction: false,
            }
        })

        await fetch('https://cloud.repup.co/api/reviews', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':Authobjects.auth_key
            },
            body: JSON.stringify({
                "replies":replies,
                "category_mentions":category_mentions,
                "date_type": "review",
                "date_range":date_range,
                "nps": nps,
                "properties":properties,
                "sources":sources,
                "tags":tags,
                "limit":limit,
                "rating":rating,
                "skip":skip
            })
        }).then((response) => response.json())
            .then((responseJson) => {

            // console.log(responseJson,'responseJson');

                dispatch ({
                    type: "GUESTFEEDBACKLIST",
                    payload: {
                        guestfeedbackdata: responseJson
                    }
                })

                dispatch ({
                    type: "LOADER",
                    payload: {
                        loadingaction: true,
                    }
                })

            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }

}


export function Properties(AuthHeader) {

    return async function (dispatch) {

        await nextFrame();

        await fetch('https://cloud.repup.co/api/properties', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':AuthHeader.auth_key
            },
        }).then((response) => response.json())
            .then((responseJson) => {

                dispatch ({
                    type: "PROPERTIES",
                    payload: {
                        Properties: responseJson
                    }
                })

            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }
}


export function Team(AuthHeader) {

    return async function (dispatch) {

        await nextFrame();

        await fetch('https://cloud.repup.co/api/team', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'auth-key':AuthHeader.auth_key
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                dispatch ({
                    type: "TEAM",
                    payload: {
                        Team: responseJson
                    }
                })
            })
            .catch((error) => {
                console.log('ERROR ON POSTING',error)
                return false;
            });
    }
}


export function Postingexample() {

    return async function (dispatch) {

        fetch('https://private-8cd41-commentpost.apiary-mock.com/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: 'Hubot',
                login: 'hubot',
            })
        })
    }
}


export function postFeedbackResponse(body) {

    return async function (dispatch) {
        await fetch('https://private-8cd41-commentpost.apiary-mock.com/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }).then((response) => response.json())
                .then((responseJson) => {

            console.log('success',responseJson);
                    // if(responseJson.success) {
                    //     console.log('Response Posted successs', responseJson);
                    //     return true;
                    // } else {
                    //     console.log('failed');
                    //     return false;
                    // }
                })
                .catch((error) => {
                    console.log('ERROR ON POSTING',error)
                    return false;
                });
    }

}


export function AgodaFetch() {

    return async function (dispatch) {
        try {
            let response = await fetch('https://private-05a123-agoda2.apiary-mock.com/');
            let agoda = await response.json();

            dispatch ({
                type: "AGODA",
                payload: {
                    agoda
                }
            })

        } catch (error) {
            console.error(error);
            dispatch ({
                type: "ERROR",
                payload: {
                    Restdata: error
                }
            })
        }
    }
}



export function GoibiboFetch() {

    return async function (dispatch) {
        try {
            let response = await fetch('https://private-202f5-goibibo1.apiary-mock.com/');
            let agoda = await response.json();

            dispatch ({
                type: "GOIBIBO",
                payload: {
                    agoda
                }
            })

        } catch (error) {
            console.error(error);
            dispatch ({
                type: "ERROR",
                payload: {
                    Restdata: error
                }
            })
        }
    }
}


export function OTPpropertdetail() {

    return async function (dispatch) {
        try {
            let response = await fetch('https://private-d5a21-demohotel.apiary-mock.com/');
            let agoda = await response.json();

            dispatch ({
                type: "OTPROPERTYFEEDBACK",
                payload: {
                    agoda
                }
            })
        }
        catch (error) {
            console.error(error);
            dispatch ({
                type: "ERROR",
                payload: {
                    Restdata: error
                }
            })
        }
    }
}


export function PostingReviewsFeedbackBYOwner(body) {

        return async function (dispatch) {
            await fetch('https://private-8cd41-commentpost.apiary-mock.com/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }).then((response) => response.json())
                .then((responseJson) => {

                    console.log('success',responseJson);
                    dispatch ({
                        type: "POSTFEEDBACKREPLY",
                        payload: {
                            Success: "Posted"
                        }
                    })
                })
                .catch((error) => {
                    console.log('ERROR ON POSTING',error)
                    return false;
                });
        }

}


export function ClosedIssue() {

    return async function (dispatch) {
        try {
            let response = await fetch('https://private-f80af-closedissues.apiary-mock.com/');
            let Issue = await response.json();

            dispatch ({
                type: "CLOSEDISSUE",
                payload: {
                    Issue
                }
            })

        } catch (error) {
            console.error(error);
            dispatch ({
                type: "ERROR",
                payload: {
                    Restdata: error
                }
            })
        }
    }
}



// export function AssignedTo() {
//
//         return async function (dispatch) {
//             try {
//                 let response = await fetch('https://private-63e497-assignedtokomaldeep.apiary-mock.com/');
//                 let Assigned = await response.json();
//
//                 dispatch ({
//                     type: "ASSIGNEDTO",
//                     payload: {
//                         Assigne: Assigned
//                     }
//                 })
//             } catch (error) {
//                 console.error(error);
//                 dispatch ({
//                     type: "ERROR",
//                     payload: {
//                         Restdata: error
//                     }
//                 })
//             }
//         }
// }


// export function Closeissuesurity(body) {
//
//     return async function (dispatch) {
//         await fetch('https://private-8cd41-commentpost.apiary-mock.com/', {
//             method: 'POST',
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json',
//             },
//             body: JSON.stringify(body)
//         }).then((response) => response.json())
//             .then((responseJson) => {
//
//                 console.log('success',responseJson);
//                 dispatch ({
//                     type: "SURITYTOCLOSEISSUE",
//                     payload: {
//                         Success: "Posted"
//                     }
//                 })
//             })
//             .catch((error) => {
//                 console.log('ERROR ON POSTING',error)
//                 return false;
//             });
//     }
//
// }


// export function LoginForm(Username , password) {
//
//         return async function (dispatch) {
//             await fetch('https://cloud.repup.co/api/login', {
//                 method: 'POST',
//                 headers: {
//                     'Accept': 'application/json',
//                     'Content-Type': 'application/json',
//                 },
//                 body: JSON.stringify({
//                     "email": Username,
//                     "password": password,
//                 })
//             }).then((response) => response.json())
//                 .then((responseJson) => {
//                     console.log('success',responseJson);
//                     dispatch ({
//                         type: "SUCESSLOGIN",
//                         payload: {
//                             Success: responseJson
//                         }
//                     })
//                 })
//                 .catch((error) => {
//                     console.log('ERROR ON POSTING',error)
//                     return false;
//                 });
//         }
// }